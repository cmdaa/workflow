package io.cmdaa.fluentd.model.kubernetes

import io.circe.generic.semiauto
import io.circe.generic.auto._
import io.circe.{Decoder, Encoder, Error, Printer, parser}
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.scalatest.junit.JUnitRunner
import cats.syntax.either._

@RunWith(classOf[JUnitRunner])
class FileMappingTest  extends FlatSpec with BeforeAndAfter {

  "file mapping string" should "parse" in {
//    implicit val fooDecoder: Decoder[FileMapping.FileMapping] = semiauto.deriveDecoder
//    implicit val fooEncoder: Encoder[FileMapping.FileMapping] = semiauto.deriveEncoder

    implicit val customPrinter = Printer.spaces2.copy(dropNullValues = true)

    val str = """
                |[{"name": "testFileOne","containerLocation": "/var/log/logOne.log","physicalLocation": "/var/log/logOne.log", "readOnly": false},
                | {"name": "testFileTwo","containerLocation": "/var/log/logTwo.log","physicalLocation": "/var/log/logTwo.log", "readOnly": true}]
                |""".stripMargin
// [{"name": "test","containerLocation": "/var/log/system.log","physicalLocation": "/var/log/system.log", "readOnly": false},{"name": "test","containerLocation": "/var/log/system.log","physicalLocation": "/var/log/system.log", "readOnly": true}]
    val ret = parser.decode[List[FileMapping.FileMapping]](str).leftMap(err => err: Error).valueOr(throw _)
    println(ret)
  }

  "file and configMap mapping string" should "parse" in {
//    implicit val fooDecoder: Decoder[FileMapping.FileMapping] = semiauto.deriveDecoder
//    implicit val fooEncoder: Encoder[FileMapping.FileMapping] = semiauto.deriveEncoder

    implicit val customPrinter = Printer.spaces2.copy(dropNullValues = true)


// [ {"name": "testConfigMapOne", "containerLocation": "dd", "configMapName": "dddd", "configMapKey": "ddddd", "newPath": "dddd", "readOnly": true } ]

// [{"name": "testConfigMapOne","containerLocation": "","configMapName": "","configMapKey": "","newPath": "",
//  "readOnly": true
//},{
//  "name": "testConfigMapTwo",
//  "containerLocation": "",
//  "configMapName": "",
//  "configMapKey": "",
//  "newPath": "",
//  "readOnly": true
//
//}]
    val str = """
                |[{
                |  "name": "testConfigMapOne",
                |  "containerLocation": "",
                |  "configMapName": "",
                |  "configMapKey": "",
                |  "newPath": "",
                |  "readOnly": true
                |},{
                |  "name": "testConfigMapTwo",
                |  "containerLocation": "",
                |  "configMapName": "",
                |  "configMapKey": "",
                |  "newPath": "",
                |  "readOnly": true
                |
                |}]
                |""".stripMargin

//    val ret = parser.decode[List[FileMapping.FileMapping]](str).leftMap(err => err: Error).valueOr(throw _)

    //  case class FileMappingConfigMap( name: String, containerLocation: String, configMapName: String, configMapKey: String, newPath: String, readOnly: Boolean = true)


    val ret = parser.decode[ List[FileMapping.FileMappingConfigMap]](str).leftMap(err => err: Error).valueOr(throw _)

    ret.foreach( println )
    assert( ret.size == 2 )
//    println(ret)
  }

}
