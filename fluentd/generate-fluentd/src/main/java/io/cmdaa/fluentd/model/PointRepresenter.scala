package io.cmdaa.fluentd.model

import org.yaml.snakeyaml.introspector.Property
import org.yaml.snakeyaml.nodes.{NodeTuple, Tag}
import org.yaml.snakeyaml.representer.Representer

class PointRepresenter extends Representer {
  override protected def representJavaBeanProperty(javaBean: Any, property: Property, propertyValue: Any, customTag: Tag): NodeTuple = { // if value of property is null, ignore it.
    if (propertyValue == null) null
    else super.representJavaBeanProperty(javaBean, property, propertyValue, customTag)
  }
}