package io.cmdaa.mongo

import com.mongodb.client.{MongoClient, MongoClients, MongoCollection, MongoDatabase}
import com.mongodb.{ConnectionString, MongoClientSettings}
import io.cmdaa.fluentd.mongo.impl.PullGroksFromMongo
import io.cmdaa.fluentd.mongo.params.fluent.DaemonSet
import org.bson.Document
import org.bson.types.ObjectId
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.scalatest.junit.JUnitRunner
import play.api.libs.json._

import collection.JavaConverters._
import java.io.PrintWriter

@RunWith(classOf[JUnitRunner])
class MongoDb extends FlatSpec with BeforeAndAfter {

  def setEnv(key: String, value: String) = {
    val field = System.getenv().getClass.getDeclaredField("m")
    field.setAccessible(true)
    val map = field.get(System.getenv()).asInstanceOf[java.util.Map[java.lang.String, java.lang.String]]
    map.put(key, value)
  }

  "ffff" should "bbbb" in {
//     val pullGroks = new PullGroksFromMongo().run("matt.test", "60e614f8220000610044576f","60e614f8220000610044576e")
    val path = "/home/matt/temp/workflow/fluentd/generate-fluentd/target"

    setEnv("LOG_ID", "61293538210000550370e313")
    setEnv("GET_RUN_INFO", "true")
    setEnv("RUN_ID", "61293538210000550370e312")

    setEnv("SYS_FILE", s"${path}/input-file.txt")
    setEnv("SYS_FILE_PATH", s"${path}/input-path.txt")
    setEnv("NODE_LBLS", s"${path}/node-labels.txt")
    setEnv("DAEMON_PREFIX", s"${path}/daemon-prefix.txt")
    setEnv("LOG_LIKELI", s"${path}/ll.txt")
    setEnv("COS_SIM", s"${path}/cos.txt")
    setEnv("NUM_WORKERS", s"${path}/workers.txt")

    setEnv("S3_PATH", s"${path}/s3-path.txt")
    setEnv("S3_BUCKET", s"${path}/s3-bucket.txt")
    DaemonSet.getLogInfo()
  }
}