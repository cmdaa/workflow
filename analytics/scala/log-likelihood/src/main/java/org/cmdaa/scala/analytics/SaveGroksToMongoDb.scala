package org.cmdaa.scala.analytics

import java.io.{File, PrintWriter}
import java.util.UUID
import java.util.concurrent.TimeUnit
import org.cmdaa.pojos.LogLikelihoodLine
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.utils.grok.GrokDetector
import org.cmdaa.utils.subsequence.LongestCommonSubsequence

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.util.Random
import io.cmdaa.gs.model.{CmdaaLog, DocMeta, Grok}
import org.apache.commons.lang3.time.StopWatch

import scala.collection.mutable._
import net.liftweb.json._
import net.liftweb.json.Serialization.{write, writePretty}

import scala.collection.mutable.HashSet
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}

/**
 * Created on 3/6/18.
 */
class SaveGroksToMongoConf(arguments: Seq[String]) extends LogLikelihoodConf(arguments) {
  val hashes = opt[String](required = true)
  val s3File = opt[String](required = true)
  val bucket = opt[String](required = true)
  val cosineSim = opt[Double](required = true)
}

  object SaveGroksToMongo {
    val mongoUri = scala.util.Properties.envOrElse("DATABASE_URI", "mongodb://cmdaa:cmdaa@mongodb:27017/cmdaa" )

    var mappedValues = new mutable.HashMap[Int, LogLikelihoodLine]()
    var conf: SaveGroksToMongoConf = _
    val grokDetector = new GrokDetector( scala.util.Properties.envOrElse( "GROK_CONF_DIR","/cmdaa/conf/groks" ) )

    def run(args: Array[String]): Unit = {
      conf = new SaveGroksToMongoConf(args)

      val s3Bucket = conf.bucket.get.get
      val s3OutputOutFile = conf.s3File.get.get

      val connectionSW = StopWatch.createStarted()
      println("Connecting to Mongo")
      connectionSW.stop()
      println(s"Connection made in ${connectionSW.getTime(TimeUnit.SECONDS)}")

      println("Creating dao")
      val dbSW = StopWatch.createStarted()
      dbSW.stop()
      println(s"dao created in ${dbSW.getTime(TimeUnit.SECONDS)}")

      createCmdaaLogRec( CmdaaLog( DocMeta( createdBy = "", updatedBy = ""), owner = "", bucket = s3Bucket, path = s3OutputOutFile ) )
    }

    case class JsonWritableGrok(uuid: String,  grok: String, logs: List[String])
    case class JsonWritableGrokList(groks: List[JsonWritableGrok])

    def createCmdaaLogRec(cmdaaLog: CmdaaLog): Unit = {
      val grokList = new ListBuffer[Grok]
      val writableGroks = new ListBuffer[JsonWritableGrok]

      val uuids = HashSet[String]()

      var logHashes = mutable.Map[String, String]()
      Source.
        fromFile(conf.hashes.apply(), "UTF-8").
        getLines().
        foreach(in => {
          val sp = in.split("\\t")
          if (sp.length == 2) {
            logHashes.put(sp(0).trim, sp(1).trim)
          }
        })

      val llResults = new LogLikelihood().run(conf)

      println(s"Log likelihood analytic complete for ${conf.analyticName.apply()}.")
      println(s"size = ${logHashes.size}")

      val results = llResults.lines.toStream.par.map(f => {
        val in = Await.result(f, Duration.Inf)
        in.clusterId = logHashes.get(in.hash).get
        in
      }).toList

      val groupedVals = results.groupBy(in => {
        in.clusterId
      })

      try {
        groupedVals.par.foreach(in => {

          val logMsgs = ListBuffer[GrokLogessagesOutFile]()
          val stringLogMsgs = ListBuffer[String]()
          println("Group: ")
          val ret = Random.shuffle(in._2).take(10)


          var groks = ListBuffer[String]()

          println("Grok detector begin")
          ret.par.foreach(out => {
 //           println(s"\t ${out.originalMessage}")

            val ffff = grokDetector.getFormatted(grokDetector.detectAllGroks(ret(0).originalMessage))
            groks.synchronized{
              groks += ffff
            }
          })
          println("Grok detector end")

          var l = LongestCommonSubsequence
          var tempGrok = ""

          println("Grok longest common subsequence begin")
          if (!groks.isEmpty) {
            tempGrok = groks(0)
            groks.par.foreach(in => {
              l.synchronized {
                tempGrok = l.find(tempGrok, in)
              }
            })
          }
          println("Grok longest common subsequence end")

          val grok = new GrokOutFile(tempGrok)

          println("")
          ret.par.foreach(out => {
            logMsgs.synchronized {
              logMsgs += GrokLogessagesOutFile(out.originalMessage)
            }
            stringLogMsgs.synchronized {
              stringLogMsgs += out.originalMessage
            }
          })


          val uuid = UUID.nameUUIDFromBytes( grok.grok.getBytes() ).toString

          println("uuids begin")
          if( !uuids.contains( uuid ) ){
            grokList.synchronized {
              grokList += Grok(grok.grok, true, stringLogMsgs.toList)
            }
            writableGroks.synchronized {
              writableGroks += JsonWritableGrok( uuid, grok.grok, stringLogMsgs.toList )
            }
            uuids.add( uuid )
          }
          println("uuids end")

          println(s"grok = ${grok}")
        })

        val runId = scala.util.Properties.envOrElse("RUN_ID", "" )
        println(s"***************************** RUN_ID = ${runId}")
        val jsonGroks = JsonWritableGrokList( writableGroks.toList )

        try {
          implicit val formats = DefaultFormats

          val json = write(jsonGroks)
          val jsonPretty = writePretty(jsonGroks)
          println("TRY 1")
          println("**************** GROKRUN \n" + json)
          println("**************** GROKRUNPRETTY \n" + jsonPretty + "**************** GROKRUNPRETTY \n")

          writeToFile(json, "/tmp/grokRun.json")
        } catch {
          case exception: Exception => exception.printStackTrace()
        }

       } catch {
        case exception: Exception => exception.printStackTrace()
      }
    }

    def writeToFile(value: String, file: String) = {
      val pw = new PrintWriter(new File(s"${file}"))
      pw.write(value)
      pw.close()
    }

    def main(args: Array[String]): Unit = {
      val log = SaveGroksToMongo

      println("begin run")
      log.run(args)
      println("end run")

      System.exit(0)
    }
}