package io.cmdaa.fluentd.mongo.impl

//import io.cmdaa.fluentd.mongo.impl.PullGroksFromMongo.kafkaInfo
import org.rogach.scallop.ScallopConf

class GenerateFluentConfigConf( arguments: Seq[String] ) extends ScallopConf ( arguments ) {
  val grokConfig = opt[String](required = true)
  val outputDirectory = opt[String](required = true)
  val analyticName = opt[String](required = true)
}

case class Grok( uuid: String, grok: String)
case class GrokList( groks: List[Grok] )

case class ElasticInfo(active: Boolean = false, url: String)
case class KafkaInfo(active: Boolean, url: String, topic: String)
case class GrokOutput( stdOut: Boolean, elastic: ElasticInfo, kafka: KafkaInfo)

case class FluentGrok(path: String, positionFile: String, tag: String, groks: List[Grok]){
  val start = getCommonStartOfLine(groks)

  def toXml: String = {
    s"<source>\n" +
      s"  @type tail\n" +
      s"  read_from_head true\n" +
      s"  path ${path}\n" +
      s"  pos_file ${positionFile}\n" +
      s"  tag ${tag}\n" +
      s"  encoding UTF-8\n" +
      s"  <parse>\n" +
      s"    @type none\n" +
      s"  </parse>\n" +
      s"</source>\n" +
      filter(groks) +
      captureServername +
      captureLogName +
      toEs +
      toKafka +
      system +
      stdout
  }

/*

<filter access>
  @type record_transformer
  <record>
    hostname ${hostname}
  </record>
</filter>
*/
//def captureServername(): String = {
//  "<filter **>\n" +
//  "  @type record_transformer\n" +
//  "  <record>\n" +
//  "    hostname \"${hostname}\"\n" +
//  "  </record>\n" +
//  "</filter>\n"
//}

def captureServername(): String = {
  val es = sys.env.getOrElse("ES_ENDPOINT", "")

  if( "".equals( es ) ) {
    "<filter **>\n" +
      "  @type record_transformer\n" +
      "  enable_ruby\n" +
      "  <record>\n" +
      "    hostname  \"#{ENV['K8S_NODE_NAME']}\"\n" +
      "  </record>\n" +
      "</filter>\n"
  }else {
    ""
  }
}

def captureLogName(): String = {
  val es = sys.env.getOrElse("ES_ENDPOINT", "")

  if( "".equals( es ) ) {
    "<filter **>\n" +
      "  @type record_transformer\n" +
      "  enable_ruby\n" +
      "  <record>\n" +
      "    logname  \"#{ENV['LOG_NAME']}\"\n" +
      "  </record>\n" +
      "</filter>\n"
  }else {
    ""
  }
}

//  <filter kubernetes.**>
//    @type kubernetes_metadata
//  </filter>
def captureKubernetes(): String = {
  "<filter **>\n" +
  "  @type kubernetes_metadata\n" +
  "</filter>\n"
}


/*
  enable_ruby
  <record>
    hostname ENV["K8S_NODE_NAME"]
  </record>
*/

  def getCommonStartOfLine(groks: List[Grok]) : String = {
    val f = groks.map(_.grok.split(" ").head).groupBy(l => l).mapValues(_.size)
    val g = f.maxBy(_._2)
    g._1
  }

  def filter(groks: List[Grok]): String = {
//    s"<filter **>\n" +
//    s"  @type concat\n" +
//    s"  key message\n" +
//    s"  multiline_start_regexp /^[^\\s]|^Caused by|^(?:(?:(?:[a-z0-9])+\\.)+(?:[A-Z\\$_]{1,1}[a-zA-Z\\$_0-9]*){1,1})/\n" +
//    s"</filter>\n" +
    s"<filter **>\n" +
    s"  @type parser\n" +
    s"  key_name message\n" +
    s"  reserve_data true\n" +
    s"  <parse>\n" +
    s"    @type multiline_grok\n" +
    s"    grok_name_key grok_name\n" +
    s"    grok_failure_key grokfailure\n\n" +
    s"    custom_pattern_path /root/groks\n" +
    s"${(groks.sortWith(_.grok.length > _.grok.length) map toElement).mkString("\n")}" +
    s"  </parse>\n" +
    s"</filter>\n"
  }

  def isValidGrok(grok: Grok): Boolean = {
    if( !( grok.grok.contains("%{") && grok.grok.contains("}") ) ) {
      return false
    }
//    if( grok.grok.trim.startsWith("at") ) {
//      println("************************************ line starts with at")
//      return false
//    }
//    if( !grok.grok.trim.startsWith(start) ){
//      return false
//    }
    true
  }

  def toElement(grok: Grok): String = {
     if( isValidGrok( grok ) ) {
        s"    <grok>\n" +
        s"      name ${grok.uuid}\n" +
        s"      pattern ${grok.grok}\n" +
        s"    </grok>\n"
      }else {
        ""
      }
  }

  val kafkaInfo: ((String, String)) => Option[KafkaInfo] = {
    case (url, topic) => {
      if(url.length > 0 && topic.length > 0) Some(KafkaInfo(true, url, topic))
      else None
    }
  }

  val elasticInfo: ((String)) => Option[ElasticInfo] = {
    case (url) => {
      if(url.length > 0) Some(ElasticInfo(true, url))
      else None
    }
  }

  def toKafka(): String = {

    val kafkaInstance = kafkaInfo( sys.env.getOrElse("KAFKA_ENDPOINT", ""), sys.env.getOrElse("KAFKA_TOPIC", "") )

    if( kafkaInstance.getOrElse(KafkaInfo(false, "", "")).active ) {
        s"<match **>\n" +
        s"  @type kafka2  # list of seed brokers\n" +
        s"  brokers ${kafkaInstance.get.url}\n" +                              //kafka-cp-kafka-headless:9092
        s"  use_event_time true  # buffer settings\n" +
        s"  <buffer fluentd>\n" +
        s"    @type file\n" +
        s"    path /var/log/td-agent/buffer/td\n" +
        s"    flush_mode ${scala.util.Properties.envOrElse("KAFKA_FLUSH_MODE", "default")}\n" +
        s"    flush_interval ${scala.util.Properties.envOrElse("KAFKA_FLUSH_TIME", "3s")}\n" +
        s"    flush_at_shutdown ${scala.util.Properties.envOrElse("KAFKA_FLUSH_SHUTDOWN", "false")}\n" +
        s"    chunk_limit_records ${scala.util.Properties.envOrElse("KAFKA_CHUNK_LIMIT", "10000")}\n"  +
        s"  </buffer>  # data type settings\n" +
        s"  <format>\n" +
        s"    @type json\n" +
        s"  </format>  # topic settings\n" +
        s"  topic_key ${kafkaInstance.get.topic}\n" +                            //fluentd
        s"  default_topic ${kafkaInstance.get.topic}\n" +   //fluentd
        s"  required_acks -1\n" +
        s"compression_codec gzip\n" +
        s"  </match>\n"
    }else {""}
  }

  def toEs(): String = {
    val esInstance = elasticInfo( sys.env.getOrElse("ES_ENDPOINT", "") )

    if ( esInstance.getOrElse(ElasticInfo(false, "")).active ) {
      s"<match **>\n" +
      s"  @type forward\n" +
      s"  <server>\n" +
      s"    host ${esInstance.get.url}\n" +
      s"    port 24224\n" +
      s"  </server>\n" +
      s"</match>\n"
    } else {""}
  }

  def system(): String = {
    s"<system>\n" +
    s"  workers ${scala.util.Properties.envOrElse("COLLECTION_THREADS", "1")}\n" +
    s"</system>\n"
  }

  def stdout(): String = {
      s"<match **>\n" +
      s"  @type stdout\n" +
      s"</match>\n"
  }

}