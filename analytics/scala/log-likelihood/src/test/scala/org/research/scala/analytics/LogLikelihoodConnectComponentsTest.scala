package org.cmdaa.scala.analytics

import com.google.gson.GsonBuilder
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterAll
import org.scalatest.FunSuite
import scalax.collection.mutable.Graph
import scalax.collection.GraphEdge.UnDiEdge
import org.junit.Assert._
import java.io.File
import scala.collection.mutable.ArrayBuffer

/**
  * Created on 1/18/18.
  */
@RunWith(classOf[JUnitRunner])
class LogLikelihoodConnectComponentsTest extends FunSuite with BeforeAndAfterAll {
  var graph: Graph[Int, UnDiEdge] = _

  override def beforeAll = {
    val buffer = new ArrayBuffer[String]
    buffer.append("-i")
    buffer.append("src/test/resources/sample-messages")
    buffer.append("-o")
    buffer.append("src/test/resources/")
    buffer.append("-c")
    buffer.append("0.90")
    buffer.append("-l")
    buffer.append("0.067")
    buffer.append("--analytic-name")
    buffer.append("test-analytic")
    buffer.append("--remove-non-alpha")
    val params = buffer.toArray

    graph = LogLikelihoodConnectComponents.run(params)
    LogLikelihoodConnectComponents.main(params)
  }

  test("test basic graph attributes") {
    //Test overall graph
    assertFalse(graph.isDirected)
    assertFalse(graph.isCyclic)
    assertFalse(graph.isConnected)
    assertFalse(graph.isEmpty)
    assertFalse(graph.isComplete)

    //Test random nodes and edges
    assertNotNull(graph.anyNode.value)
    assertTrue(graph.anyEdge.isNode)
  }

  test("test component traversal") {
    val traverser = graph.componentTraverser()
    assertEquals(traverser.size, 11)
  }

  test("test output files") {
    assertTrue(new File("src/test/resources/test-analytic-clusters.out").exists())
    assertTrue(new File("src/test/resources/test-analytic.out").exists())
  }
}