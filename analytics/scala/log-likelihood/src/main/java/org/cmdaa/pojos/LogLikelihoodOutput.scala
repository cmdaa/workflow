package org.cmdaa.pojos

import scala.concurrent.Future

/**
  * Created on 2/1/18.
  */
class LogLikelihoodOutput {
  var lines:Iterator[Future[LogLikelihoodLine]] = null
  var totalMaxLikelihood = 0
  var totalNaN = 0
  var totalLogLines = 0
  var totalWords = 0
  var totalLlVals = 0d
}
