# Running LogLikely within Docker
Make sure that you have installed [Docker](https://www.docker.com/).  

Simply Run:

`docker run -e ANALYTIC="org.cmdaa.scala.analytics.LogLikelihoodConnectComponents" -e PARAMETERS="-a hadoop -i /analytic-input/* -o /tmp -l 0.05 -c 0.90" -e JAVA_OPTS="-Xms16g -Xmx16g" -v /my/output/dir:/tmp -v /my/input/dir:/analytic-input cmdaa/log-likelihood:0.0.1`


### ARGS - 
  * a = the name or label that you are giving the analytic run of this data set.
  * i = /analytic-input - this is the default directory that the analytics will process(within the docker image)
  * o = the output directory within the docker image.  The default is temp.
  * l = the log likelihood threshold.  This is the statistical threshold that you are setting to generate classes of log files.
    1.  The higher you make this, the more tokens are allowed in to a log message template.
    2.  The lower this value the less tokens will be included in the log templates generated.
  * c = The cosine similarity threshold.  This is the threshold for creating classes of log messages. [See Definition](https://en.wikipedia.org/wiki/Cosine_similarity)

### Volumes - 
  * output - the output directory that you would like the analytic to write to
  * input - the input directory that you would like the analytic to read from 

## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/