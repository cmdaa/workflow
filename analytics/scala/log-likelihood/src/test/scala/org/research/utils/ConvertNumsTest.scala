package org.research.utils

import org.cmdaa.utils.{ConvertNums, Features}
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import org.junit.Assert._
import org.junit.runner.RunWith
import org.cmdaa.utils.ConvertNums._

@RunWith(classOf[JUnitRunner])
class ConvertNumsTest extends FlatSpec {

  "tens" should "should return 0 thruogh 9" in {

 //   assert( convert(0 ) == "zero" )
    assert( convert(1 ) == "One" )
    assert( convert(2 ) == "Two" )
    assert( convert(3 ) == "Three" )
    assert( convert(4 ) == "Four" )
    assert( convert(5 ) == "Five" )
    assert( convert(6 ) == "Six" )
    assert( convert(7 ) == "Seven" )
    assert( convert(8 ) == "Eight" )
    assert( convert(9 ) == "Nine" )

    println(convert( 192 ).replaceAll(" ", ""))

  }

}
