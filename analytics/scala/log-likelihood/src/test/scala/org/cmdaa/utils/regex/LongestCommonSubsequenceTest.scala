package org.cmdaa.utils.regex


import org.cmdaa.utils.subsequence.LongestCommonSubsequence
import org.scalatest.FlatSpec

class LongestCommonSubsequenceTest extends FlatSpec {

  "find" should "find a subsequence" in {
    var l = LongestCommonSubsequence
    assert( l.find("this is some stuff here this a test", "this foo this actualy works  a test") == "this this a test" )
  }

  "fizz" should "not find a subsequence" in {
    var l  = LongestCommonSubsequence
    assert( l.find( "no matches in these strings", "some value here that will not") == "" )
  }

  "test" should "foo bar baz" in {
    var l  = LongestCommonSubsequence

    val string1 = "NUM/NUM/NUM TIME LOGLEVEL JAVACLASS: Registered signal handlers for [TERM, HUP, INT]"
    val string2 = "NUM/NUM/NUM TIME LOGLEVEL JAVACLASS: Starting executor ID NUM on host mesos-slave-NUM"
    val string3 = "NUM/NUM/NUM TIME LOGLEVEL JAVACLASS: Server created on NUM"

    println("TEST 1")
    println(s"\t${l.find(string1, string2)}")

    println("TEST 2")
    println(s"\t${l.find(string2, string3)}")

    println("TEST 3")
    println(s"\t${l.find(string1, string3)}")


    var result = l.find(string1, string2)
    result = l.find(result, string3)

    println("RESULTS =")
    println(s"\t${result}")
  }

  "deprecated" should "foo bar baz" in {
    var l  = LongestCommonSubsequence

    val string1 = "NUM/NUM/NUM TIME LOGLEVEL Configuration.deprecation: mapred.tip.id is deprecated. Instead, use mapreduce.task.id"
    val string2 = "NUM/NUM/NUM TIME LOGLEVEL Configuration.deprecation: mapred.task.partition is deprecated. Instead, use mapreduce.task.partition"
    val string3 = "NUM/NUM/NUM TIME LOGLEVEL Configuration.deprecation: mapred.task.is.map is deprecated. Instead, use mapreduce.task.ismap"
    val string4 = "NUM/NUM/NUM TIME LOGLEVEL Configuration.deprecation: mapred.task.id is deprecated. Instead, use mapreduce.task.attempt.id"

    val strList = List(string1, string2, string3, string4)

    var ret = l.find(string1, string2)
    strList.foreach( in => {
      ret = l.find(ret, in)
    })

    println("RETURN:")
    println(s"\t${ret}")
  }
}