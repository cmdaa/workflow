# CMDAA application workflow architecture
![Install Docker Image](images/workflow-architecture.png)

## Argo
Argo is installed using the argo [Helm Chart](https://github.com/argoproj/argo-helm).  For more information on Argo, browse [here](https://github.com/argoproj/argo)

It contains:<br>
  1 - The argo CLI - A mechanism to submit workflows defined by static YML files<br>
  ```
  argo submit --watch https://raw.githubusercontent.com/argoproj/argo/master/examples/hello-world.yaml
  ```
  2 - Argo UI - A mechanism to see the status of running workflows<br>
  ![Install Docker Image](images/argo-ui.png)
  3 - Argo workflow controller - Allows the CLI to submit workflows and handles communication with the Argo UI and kube API<br>
  
## CMDAA - Workflow
The CMDAA application uses the argo workflow framework for it's workflows.  The 
CMDAA project requires a mechanism to run dynamic workflows within argo.  At the
time of it's implementation, there was no mechanism within Argo that would allow
this. This may have changed with the advent of argo-events.  

The CMDAA application has various rest end endpoints that a user can use to run
a workflow.

These Include:
   1.  Run predefined workflows that have been defined by CMDAA.
   2.  Run workflows that have been defined by you.

## Rest Endpoints
The rest endpoints allow the UI to run workflows on demand.  It also allows 
scheduled workflows to run within [Kube CronJobs](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/).

## Running workflows
You can also run any workflow that you desire by calling the workflow rest endpoint 
with a properly formatted argo workflow.  The json payload will be converted to a
YAML file within the pod and run by the Argo workflow engine.


Once the CMDAA workflow API is called, it will make a call to the Kubernetes API
and run the CMDAA Argo workflow driver pod.  This preconfigured workflow or dynamic
workflow generates a yaml file that is ultimately mapped into the Argo driver pod.

The Argo Driver pod will spin up and submit the predefined or dynamic workflow to
the kubernetes API.  Once the driver pod submits the workflow, it will spin down.

The workflow that is spun up by the Argo driver pod can me monitored by the Argo
application like traditional workflows.

## What happens when you call the CMDAA Argo workflow service API?
1.  The CMDAA workflow API parses the requested workflow
2.  The CMDAA application calls the K8s API and spins up an Argo worflow driver pod.
3.  The Argo driver pod spins up and makes a request to the kubernetes API, on behalf of Argo, to run the requested workflow.
4.  When the workflow is submitted, the cmdaa argo driver spins down.
5.  The Argo workflow engine takes over. 
6.  The workflow engine runs the desired workflow.  Each step in the workflow is spun up as an individual pod.
7.  Intermediary data formats are stored in S3 and used by subsequent stages.  








