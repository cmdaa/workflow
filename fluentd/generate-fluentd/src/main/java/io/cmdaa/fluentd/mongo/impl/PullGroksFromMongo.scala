package io.cmdaa.fluentd.mongo.impl

import java.io.{File, PrintWriter}
import java.util.UUID

import io.cmdaa.gs.dao.{CmdaaLogDao, MongoComponents}
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class PullGroksFromMongo() {

  case class MongDBConnection(val asyncDriver: AsyncDriver, val connection: MongoConnection, val database: Future[DB]) extends MongoComponents {}

  def fromUri(mongoUri: String): Future[MongDBConnection] = {
    val driver = new AsyncDriver

    for {
      uri <- MongoConnection.fromString(mongoUri)
      con <- driver.connect(uri)
    } yield MongDBConnection(driver, con, con.database(uri.db.get))
  }

  def run(outputFile: String, logId: String, runId: String): Unit = {
    val mongoUri = scala.util.Properties.envOrElse("DATABASE_URL", "mongodb://cmdaa:cmdaa@mongodb:27017/cmdaa")
    val inputFile = scala.util.Properties.envOrElse("INPUT_FILE", "/root/input.log")
    val positionFile = s"${inputFile}.pos"

    val f_mongoComponents = fromUri(mongoUri)

    val testT1 = f_mongoComponents.map(c => new CmdaaLogDao(c)).
      flatMap {
        dao => dao.findByLogId( logId )
      }

    val testT1Result = Await.result(testT1, Duration.Inf).get

    println(s"bucket = ${testT1Result.bucket}")
    println(s"path = ${testT1Result.path}")
    println(s"owner = ${testT1Result.owner}")
    println(s"createdBy = ${testT1Result.meta.createdBy}")
    println(s"runs size = ${testT1Result.runs.size}")

    testT1Result.runs.foreach( in => {
      println(s"id = ${in._id.stringify}")
      println(s"groks size = ${in.groks.size}")
      println(s"passed in id = ${runId}")
      println(s"boolean equals ${in._id.stringify.equals(runId)}")
    })

    val runRes = testT1Result.runs.filter(_._id.stringify.equals(runId))

    println(s"runRes size = ${runRes.size}")
    val run = runRes.head

    val notParRes = run.groks.map( grok => {
      Grok(UUID.nameUUIDFromBytes(grok.grok.getBytes()).toString, grok.grok)
    })

    println(s"notParRes size = ${notParRes.size}")

    val xml = FluentGrok( inputFile,
                          positionFile,
                          "hadoop_grok_log",
                          groks = notParRes)
    val p = new scala.xml.PrettyPrinter(1000, 2)

    println("==============================================\n" + xml.toXml.toString + "\n==============================================\n")

    val pw = new PrintWriter(new File(s"${outputFile}"))
    pw.write(xml.toXml.toString)
    pw.close()
  }
}

object PullGroksFromMongo {

  def main(args: Array[String]): Unit = {
    try {

      val output = System.getenv("OUTPUT_FILE")
      val runId = System.getenv( "RUN_ID")
      val logId = System.getenv( "LOG_ID")

      println(s"output = ${output}\nlogId = ${logId}\nrunId = ${runId}\n")

      new PullGroksFromMongo().run( output, logId, runId )

      val currentGroup = Thread.currentThread.getThreadGroup
      val noThreads = currentGroup.activeCount
      val lstThreads = new Array[Thread](noThreads)
      currentGroup.enumerate(lstThreads)

      try {
        for (i <- 0 until noThreads) {
          System.out.println("Thread No:" + i + " = " + lstThreads(i).getName)

          if (!lstThreads(i).getName.equalsIgnoreCase("main")) {
            lstThreads(i).interrupt()
          }
        }
      } catch {
        case e: Exception => e.printStackTrace()
      }

      System.exit(0)

    } catch {
      case e: Exception => {
        e.printStackTrace()
      }
    }
  }
}