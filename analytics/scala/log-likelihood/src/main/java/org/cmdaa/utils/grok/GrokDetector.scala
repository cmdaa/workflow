package org.cmdaa.utils.grok

import io.krakens.grok.api.{Grok, GrokCompiler}

import scala.io.Source
import scala.collection.JavaConverters._
import org.cmdaa.utils.ConvertNums._

class GrokDetector(grokDirectory: String = new java.io.File(".").getCanonicalPath + "/src/main/resources") {

  val grokCompiler = GrokCompiler.newInstance()
  grokCompiler.registerDefaultPatterns()

  println(s"${grokDirectory}")

  try {
    grokCompiler.register(Source.fromFile(s"${grokDirectory}/dates.grok").reader())
    grokCompiler.register(Source.fromFile(s"${grokDirectory}/java.grok").reader())
    grokCompiler.register(Source.fromFile(s"${grokDirectory}/numbers.grok").reader())
    grokCompiler.register(Source.fromFile(s"${grokDirectory}/custom.grok").reader())
    grokCompiler.register("NOT_GROK_BEGIN", "[^%\\{].*")
    grokCompiler.register("NOT_GROK_END", ".*[^\\}]")
  } catch {
    case e: Exception => e.printStackTrace()
  }

  def getCompiler(): GrokCompiler = { grokCompiler }

  val patterns = grokCompiler.getPatternDefinitions

  val customGroks = loadCustomGroks().toList

  val logFormats = "SYSLOGBASE" -> grokCompiler.compile(s"%{SYSLOGBASE}") ::
                    "COMMONAPACHELOG" -> grokCompiler.compile(s"%{COMMONAPACHELOG}") ::
                    "COMBINEDAPACHELOG" -> grokCompiler.compile(s"%{COMBINEDAPACHELOG}") ::
                    Nil

  val dateGroks = "TIME" -> grokCompiler.compile(s"%{TIME}") ::
                  "TIME_WITH_MS" -> grokCompiler.compile(s"%{TIME_WITH_MS}") ::
                  "YEAR_MONTH_DATE_US_TIME" -> grokCompiler.compile(s"%{YEAR_MONTH_DATE_US_TIME}") ::
                  "YEAR_MONTH_DATE_US_TIME_MS" -> grokCompiler.compile(s"%{YEAR_MONTH_DATE_US_TIME_MS}") ::
                  "YEAR_MONTH_DATE_TIME_W_MS" -> grokCompiler.compile(s"%{YEAR_MONTH_DATE_TIME_W_MS}") ::
                  "TIMESTAMP_ISO8601" -> grokCompiler.compile(s"%{TIMESTAMP_ISO8601}") ::
                  "TS_ISO8601_SLASHES" -> grokCompiler.compile(s"%{TS_ISO8601_SLASHES}") ::
                  "DATESTAMP_RFC822" -> grokCompiler.compile(s"%{DATESTAMP_RFC822}") ::
                  "DATESTAMP_RFC2822" -> grokCompiler.compile(s"%{DATESTAMP_RFC2822}") ::
                  "DATESTAMP_EVENTLOG" -> grokCompiler.compile(s"%{DATESTAMP_EVENTLOG}") ::
                  "DATESTAMP_OTHER" -> grokCompiler.compile(s"%{DATESTAMP_OTHER}") ::
                  "SYSLOGTIMESTAMP" -> grokCompiler.compile(s"%{SYSLOGTIMESTAMP}") ::
                  "DAY_MONTH_YEAR" -> grokCompiler.compile(s"%{DAY_MONTH_YEAR}") ::
                  "TIMESTAMP_ISO8601" -> grokCompiler.compile(s"%{TIMESTAMP_ISO8601}") ::
                  Nil

  val dayGroks = "DAY" -> grokCompiler.compile(s"%{DAY}") ::
                 Nil

  val internetGroks = "IPV4" -> grokCompiler.compile(s"%{IPV4}") ::
                      "IPV6" -> grokCompiler.compile(s"%{IPV6}") ::
                      "HOSTPORT" -> grokCompiler.compile(s"%{HOSTPORT}") ::
                      Nil

  val dateTimeGroks = "DATE" -> grokCompiler.compile(s"%{IPV4}") ::
                      "TIME"-> grokCompiler.compile(s"%{IPV6}") ::
                      Nil

  val javaGroks = "JAVACLASS" -> grokCompiler.compile(s"%{JAVACLASS}") ::
                   Nil

  val logLevelGroks = "LOGLEVEL" -> grokCompiler.compile(s"%{LOGLEVEL}") ::
                      Nil

    val cmdaaNumbersGroks = "POSINT" -> grokCompiler.compile(s"([ =\\[\\(])%{POSINT}([ \\]\\)$$:])") ::
                            Nil

  def getFormatted( passed: String ): String = {
        val replace = "(\\[|\\]|\\\\|\\^|\\$|\\.|\\||\\?|\\*|\\+|\\(|\\))".r

        var str = passed
        str = "\\s{2,}".r.replaceAllIn(str, " {2,}")

        val r = replace.findFirstIn(str)
        str = replace.replaceAllIn( str, m => {s"\\\\\\${m.group(1)}"})

        val regex = "(.*)(%\\{.*\\})(.*)".r
        val foo = str.split(" |\\=")

        val ret = str.split(" ").map( in => {
          var retValue = ""
          val results = regex.findFirstIn( in )
          if( results != None ) {

            val regex(pre, value, post) = in

            if(pre.length > 0 || post.length > 0){
              val preVal = if(pre.length > 0 && "[0-9]+".r.findFirstIn(pre) != None) ".*" else pre
              val postVal = if(post.length > 0 && "[0-9]+".r.findFirstIn(post) != None) ".*" else post
              retValue = s" ${preVal}${value}${postVal}"
            }
            else {
              retValue = s" ${results.get}"
            }

          }else{
            val numPattern = "[0-9]+".r
            val match1 = numPattern.findFirstIn(in)

            retValue = " .*"

            if(match1 == None){
              retValue = s" ${in}"
            }
            else if( in.contains("=") ) {
              val foo = in.split("\\=")
              if( foo.size == 2 && !foo(0).matches(".*\\d.*") && foo(1).matches(".*\\d.*") ){
                retValue = s" ${foo(0)}=.*"
              }
            }
            else {
              retValue = " .*"
            }
          }
          retValue
        } ).mkString.trim

        val foldRight = ret.replaceAll("(?:\\.\\* ?)+", ".*")
    //    val foldRight = ret.replaceAll("(?:\\s\\.\\* ?)+", ".* ")

        //    println(foldRight)
        //    println("-----------------------------------")
        foldRight.trim


  }

  def detectGrok(groks: List[(String, Grok)], input:String, varType: String = "",  currNum:Int = 1, detectNums: Boolean = false, withReplacement: Boolean = true, replaceString: Boolean = false): String = {
    var ret = input
    groks.foreach( in => {
      val gm = in._2.`match`(input).capture()
      if(gm.size() > 0){
            if( replaceString ){
              ret = detectGrok(groks,  input.replaceFirst(in._2.getNamedRegex, s"%{${in._1}:${varType}${convert(currNum)}}"), varType, currNum + 1, false, true )
            }else if( !detectNums ) {
              if( withReplacement ) {
                ret = detectGrok(groks,  input.replace(gm.get(in._1).toString, s"%{${in._1}:${varType}${convert(currNum)}}"), varType, currNum + 1, false, true )
              }else {
                ret = detectGrok(groks,  input.replace(gm.get(in._1).toString, s"${in._1}"), varType, currNum + 1, false, false )
              }
            } else {
              val reg = cmdaaNumbersGroks(0)._2.getNamedRegex
              val key = cmdaaNumbersGroks(0)._1

              if( withReplacement ) {
                ret = detectGrok(groks, input.replaceFirst(reg, "$1" + s"%{${in._1}:${varType}${convert(currNum)}}" + "$3"), varType, currNum + 1, true, true)
              }else {
                ret = detectGrok(groks, input.replaceFirst(reg, "$1" + s"${in._1}" + "$3"), varType, currNum + 1, true, false)
              }
            }
      }
    })
    return ret
  }

  def detectAllGroks(input: String, withReplacement: Boolean = true): String = {
    var ret = input
    ret = detectTime(input, withReplacement)
    ret = detectInternet(ret, withReplacement)
    ret = detectDateTime(ret, withReplacement)
    ret = detectDayOfWeek(ret, withReplacement)
    ret = detectJava(ret, withReplacement)
    ret = detectCustomGroks(ret, withReplacement)
    ret = detectLogLevel(ret, withReplacement = withReplacement)
    ret = detectCMDAANums(ret, withReplacement)
    ret
  }

  def detectLogFormat(input: String, withReplacement: Boolean = true): String = {
    detectGrok(logFormats, input, "logformat", 1, false, withReplacement)
  }

  def detectTime(input: String, withReplacement: Boolean = true): String = {
    detectGrok(dateGroks, input, "time", 1, false, withReplacement)
  }

  def detectDateTime(input: String, withReplacement: Boolean = true): String = {
    detectGrok(dateTimeGroks, input, "datetime", 1, false, withReplacement)
  }

  def detectDayOfWeek(input: String, withReplacement: Boolean = true): String = {
    detectGrok(dayGroks, input, "day", 1, false, withReplacement)
  }

  def detectInternet(input: String, withReplacement: Boolean = true): String = {
   detectGrok(internetGroks, input, "host", 1, false, withReplacement)
  }

  def detectJava(input: String, withReplacement: Boolean = true): String = {
    detectGrok(javaGroks, input, "java", 1, false, withReplacement)
  }

  def detectCMDAANums(input: String, withReplacement: Boolean = true): String = {
    detectGrok(cmdaaNumbersGroks, input, "number", 1, detectNums = true, withReplacement)
  }

  def detectCustomGroks(input: String, withReplacement: Boolean = true): String = {
    detectGrok(customGroks, input, "custom", 1, detectNums = false, withReplacement, replaceString = true)
  }

  def detectLogLevel(input: String, currNum:Int = 1, withReplacement: Boolean = true): String = {
    var ret = input
    logLevelGroks.foreach( in => {
      val gm = in._2.`match`(input).capture()
      if(gm.size() > 0){
        if(withReplacement) {
          ret = detectLogLevel(input.replace(gm.get(in._1).toString.replaceAll("[^a-zA-Z]", ""), s"%{${in._1}:loglevel${convert(currNum)}}"), currNum + 1, withReplacement)
        }
        else{
          ret = detectLogLevel(input.replace(gm.get(in._1).toString.replaceAll("[^a-zA-Z]", ""), s"${in._1}"), currNum + 1, withReplacement)
        }
      }
    })
    return ret
  }

  def loadCustomGroks(): Map[String, Grok] = {
    loadGroks(s"${grokDirectory}/custom.grok")
  }

  def loadGroks(file: String): Map[String, Grok] = {
    val groks = scala.collection.mutable.Map.empty[String, Grok]
    for (line <- Source.fromFile(file).getLines) {
      val keyVal = line.split(" ", 2)
      groks += (keyVal.head -> grokCompiler.compile(s"%{${keyVal.head}}") )
    }
    groks.toMap
  }

}