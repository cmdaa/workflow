package org.cmdaa.utils

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.junit.Assert._
import scala.collection.JavaConversions._

@RunWith(classOf[JUnitRunner])
class WordUtilsTest extends FunSuite {

  test("convert string of unique words to word counts map") {
    val words = WordUtils.generateWordCountsMap("this is only a test")

    assertEquals(words.size, 5)

    assertTrue(words.contains("this"))
    assertTrue(words.contains("is"))
    assertTrue(words.contains("a"))
    assertTrue(words.contains("test"))

    assertEquals(1, words.get("this").get)
    assertEquals(1, words.get("is").get)
    assertEquals(1, words.get("only").get)
    assertEquals(1, words.get("a").get)
    assertEquals(1, words.get("test").get)
  }

  test("convert string of unique and dupe words to word counts map") {
    val words = WordUtils.generateWordCountsMap("this is a test followed by a second test")

    assertEquals(words.size, 7)

    assertTrue(words.contains("this"))
    assertTrue(words.contains("is"))
    assertTrue(words.contains("a"))
    assertTrue(words.contains("test"))
    assertTrue(words.contains("second"))
    assertTrue(words.contains("followed"))

    assertEquals(1, words.get("this").get)
    assertEquals(1, words.get("is").get)
    assertEquals(2, words.get("a").get)
    assertEquals(2, words.get("test").get)
    assertEquals(1, words.get("followed").get)
    assertEquals(1, words.get("by").get)
  }

  test("convert string of unique, dupe, and camel-case words to word counts map") {
    val words = WordUtils.generateWordCountsMap("This is a test followed by a second TeST")

    assertEquals(words.size, 7)

    assertTrue(words.contains("this"))
    assertTrue(words.contains("is"))
    assertTrue(words.contains("a"))
    assertTrue(words.contains("test"))
    assertTrue(words.contains("followed"))
    assertTrue(words.contains("by"))
    assertTrue(words.contains("second"))

    assertEquals(1, words.get("this").get)
    assertEquals(1, words.get("is").get)
    assertEquals(2, words.get("a").get)
    assertEquals(2, words.get("test").get)
    assertEquals(1, words.get("followed").get)
    assertEquals(1, words.get("by").get)

    assertFalse(words.contains("TeST"))
  }

  test("convert string with dupes to unique words map") {
    val words = WordUtils.generateUniqueWordsMap("This is a test This is only a test.")

    assertEquals(5, words.size)

    assertEquals(1, words.get("this"))
    assertEquals(1, words.get("is"))
    assertEquals(1, words.get("only"))
    assertEquals(1, words.get("a"))
    assertEquals(1, words.get("test"))

    assertFalse(words.contains("This"))
  }

  test("convert string with camel case to unique words map") {
    val words = WordUtils.generateUniqueWordsMap("This is ONLY a test; this is only a TeST.")

    assertEquals(5, words.size)

    assertEquals(1, words.get("this"))
    assertEquals(1, words.get("is"))
    assertEquals(1, words.get("only"))
    assertEquals(1, words.get("a"))
    assertEquals(1, words.get("test"))

    assertFalse(words.contains("This"))
  }
}