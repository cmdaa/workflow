# Building the Project and Running on Bare Metal.

## Log Likely Analytic:
In order to build LogLikely, you need the [Maven build tool](https://maven.apache.org/) installed.

### Building Log Likelihood Analytic:
```
$ git clone git@gitlab.com:cmdaa/workflow.git
$ cd  workflow/analytics/scala/log-likelihood
$ mvn clean install
```

### Running Log Likelihood Analytic:
```
$ /usr/local/bin/scala -classpath target/log-likelihood.jar org.cmdaa.scala.analytics.LogLikelihoodConnectComponents -a my-analytic -i /analytic-input/* -o /tmp -l 0.05 -c 0.90
```
#### ARGS - 
  * a = the name or label that you are giving the analytic run of this data set.
  * i = /analytic-input - this is the default directory that the analytics will process(within the docker image)
  * o = the output directory within the docker image.  The default is temp.
  * l = the log likelihood threshold.  This is the statistical threshold that you are setting to generate classes of log files.
    1.  The higher you make this, the more tokens are allowed in to a log message template.
    2.  The lower this value the less tokens will be included in the log templates generated.
  * c = The cosine similarity threshold.  This is the threshold for creating classes of log messages. [See Definition](https://en.wikipedia.org/wiki/Cosine_similarity)

## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/