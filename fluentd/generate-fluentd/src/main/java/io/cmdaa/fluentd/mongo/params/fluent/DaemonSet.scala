package io.cmdaa.fluentd.mongo.params.fluent

import com.mongodb.client.{MongoClient, MongoClients, MongoCollection, MongoDatabase}
import com.mongodb.{ConnectionString, MongoClientSettings}
import org.bson.Document
import org.bson.types.ObjectId
import play.api.libs.json._

import collection.JavaConverters._
import java.io.PrintWriter

object DaemonSet {

  case class MongoInfo(mongoClient: MongoClient, mongoDatabase: MongoDatabase)

  def connectGetCollections(db: String): MongoInfo = {
    val connString = new ConnectionString( sys.env.getOrElse("MONGODB_URL", "mongodb://cmdaa:cmdaa@localhost:6969/cmdaa") )
    val settings = MongoClientSettings.builder.applyConnectionString(connString).retryWrites(true).build
    val mongoClient = MongoClients.create(settings)
    val database = mongoClient.getDatabase(db)
    database.listCollections().asScala.map( in => {
      println("*****\t collection = " + in.toJson)
    })
    MongoInfo(mongoClient, database)
  }

  def getFile(in:String):String = { in.split("/").last }
  def getDir(in:String):String = {
    val p = in.replace(s"${getFile(in)}","")
    p.split("/").mkString("/")
  }

  def getLogInfo(): Unit = {
    val mongoInfo = connectGetCollections("cmdaa")
    val collection = mongoInfo.mongoDatabase.getCollection("logRuns")

    val query = new Document("_id", new ObjectId(sys.env.getOrElse("LOG_ID", "")))

    collection.find(query).asScala.map( in => {
      def printFile(file: String, contents: String):Unit = {
        if( !file.equals("") ) new PrintWriter(file) { write(contents.replace("\"","")); close }
      }

      val json = Json.parse(in.toJson)
      println( s"********* s3 path:\n  ${json("path")}" )
      println( s"********* s3 bucket:\n  ${json("bucket")}"  )

      printFile( sys.env.getOrElse("S3_PATH", ""), json("path").as[JsString].value )
      printFile( sys.env.getOrElse("S3_DIR", ""), getDir( json("path").as[JsString].value ))
      printFile( sys.env.getOrElse("S3_FILE", ""), getFile( json("path").as[JsString].value ) )
      printFile( sys.env.getOrElse("S3_BUCKET", ""), json("bucket").as[JsString].value )

      val runs = json("runs").as[JsArray]


      def getFieldAsString(obj: JsObject, field: String): String = {
        try {
          val str = obj( field ).as[JsString].toString()
          println(s"******* field: ${field} value: ${str}.")
          str
        } catch {
          case e:Exception => {
            println(s"******* field not found: ${field} ")
            ""
          }
        }
      }

      try {
        val dsConfig = json("dsConfig").as[JsObject]

        printFile( sys.env.getOrElse( "NUM_WORKERS", "" ), getFieldAsString(dsConfig, "workers") )

        dsConfig("paths").as[JsArray].value.foreach(path => {
          val f = getFile(path.as[JsString].toString())
          val p = getDir(path.as[JsString].toString())
          println(s"******\t full path: ${path}")
          println(s"******\t dir: ${p}")
          println(s"******\t file: ${f}")
          printFile(sys.env.getOrElse("SYS_FILE", ""), f)
          printFile(sys.env.getOrElse("SYS_FILE_PATH", ""), p)
        })

        dsConfig("nodeLbls").as[JsArray].value.foreach(label => {
          println(s"******\t label: \n ${label}")
          printFile(sys.env.getOrElse("NODE_LBLS", ""), getFile(label.toString()))
        })

        printFile( sys.env.getOrElse( "DAEMON_PREFIX", "" ), getFieldAsString(dsConfig, "daemonPrefix") )

      } catch {
        case e:Exception => println("dsConfig not found")
      }


      if( sys.env.getOrElse("GET_RUN_INFO", "false").toBoolean ) {
        println("********** Pulling run info")
        runs.value.map(in => {

          val passedId = sys.env.getOrElse( "RUN_ID", "")
          println("********* PASSED ID = " + passedId)

          in("_id").as[JsObject].fieldSet.foreach(id => {
            val runId = id._2.as[JsString].toString().replace("\"","")
            println("********* ID FOUND= " + runId)
            if ( runId.equalsIgnoreCase( passedId ) ){
              val run = in.as[JsObject]
              val ll = run("logLikelihood").toString.replace("\"","").toFloat
              val cos = run("cosSimilarity").toString.replace("\"","").toFloat

              println( f"***************LL FOUND + $ll%1.2f" )
              println( f"***************COS SIM FOUND + $cos%1.2f" )

              printFile( sys.env.getOrElse("LOG_LIKELI", ""), f"$ll%1.2f" )
              printFile( sys.env.getOrElse("COS_SIM", ""), f"$cos%1.2f" )
            }
          })
        })
      }

    })
    println("closing db connection")
    mongoInfo.mongoClient.close()
    println("db connection closed")
  }

  def main(args: Array[String]): Unit = {
      DaemonSet.getLogInfo()
  }
}
