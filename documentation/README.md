## Documentation
   * [Getting Started](https://gitlab.com/cmdaa/workflow/blob/master/documentation/GETTINGSTARTED.md)
   * [Setup with Helm (Recommended!)](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLVIAHELM.md)
   * [Installing within Kubernetes](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLINGK8S.md)
   * [Setting up Docker on a Mac](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLMAC.md)
   * [Running within Docker](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLINGDOCKER.md)
   * [Running on bare metal](https://gitlab.com/cmdaa/workflow/blob/master/documentation/BUILDINGPROJECT.md)

## Sub Projects
   * [Data Pull](https://gitlab.com/cmdaa/workflow/tree/master/data-pull) - The first step in the analytic process.  This project dedicated to pulling logs from your log data source.
   * [Analytics](https://gitlab.com/cmdaa/workflow/tree/master/analytics) - The preceding steps in the analytic process.  Analytics to run against your data!!!

## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 

