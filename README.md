# LogLikely [lɔɡ-'laɪklɪ]

## Log Likelihood Definition 
  1.  A function of parameters of a statistical model, given specific observed data.

## What is LogLikely?
LogLikely is an open source analytic framework that will intelligently recognize patterns within the logs of your applications stack.  LogLikely is meant to run seamlessly within your Kubernetes ecosystem. 

   * LogLikely is built to run natively within your Kubernetes environment
   * It is a set of analytics that will:
      1.  Pull logs from your log data source.
      2.  Analyze logs and pick up patterns and trends.
      3.  Automatically suggest parsing routines that will intelligently parse your data logs to bring you quicker insight.
      4.  Is designed to work in the Argo workflow engine, but can be run in containers, or bare metal.
      5.  Work seemlessly within the GrokZilla framework.

## Why LogLikely?
   * If you're anything like us, you would prefer to spend your days doing other things at work that don't include combing through mountains of log files.
   * Wouldn't you like an intelligent set of analtyics to comb through these log files for you?
   * Better yet, wouldn't you like these analytics to detect patterns within your log files and report them to you?  
   * Identify anomolies within your log files and report them to you.
   * Run clustering algorithms on your log files and report standard classes of log messages that are occurring within your system.
   * LogLikely will work within kubernetes, docker, or even bare metal.

## Documentation
   * [Getting Started](https://gitlab.com/cmdaa/workflow/blob/master/documentation/GETTINGSTARTED.md)
   * [Installing within Kubernetes](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLINGK8S.md)
   * [Running within Docker](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLINGDOCKER.md)
   * [Running on bare metal](https://gitlab.com/cmdaa/workflow/blob/master/documentation/BUILDINGPROJECT.md)

## Sub Projects
   * [Data Pull](https://gitlab.com/cmdaa/workflow/tree/master/data-pull) - The first step in the analytic process.  This project dedicated to pulling logs from your log data source.
   * [Analytics](https://gitlab.com/cmdaa/workflow/tree/master/analytics) - The preceding steps in the analytic process.  Analytics to run against your data!!!

## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 

