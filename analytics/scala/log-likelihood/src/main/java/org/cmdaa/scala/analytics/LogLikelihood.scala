package org.cmdaa.scala.analytics

import java.text.DecimalFormat
import org.cmdaa.pojos.{LogLikelihoodLine, LogLikelihoodOutput}
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.utils.LoglikelihoodUtils

import scala.io.Source
import scala.collection.parallel.ParMap
import scala.concurrent.{ExecutionContext, Future}

/**
 * Encapsulates logic to parse a file and return a LogLikelihoodOutput object 
 * 
 * Created on 2/1/18.
 */
class LogLikelihood {
 
  /**
   * Processes incoming log file and generates a LogLikelihoodOutput object. The majority 
   * of the business logic is contained in the LogLikelihoodUtils object.
   */
  def run(conf: LogLikelihoodConf): LogLikelihoodOutput = {
    val ll = Source.fromFile(conf.inputDirectory.apply(), "UTF-8").getLines().toStream

    val wordCount = ll.
      par.
      flatMap( LoglikelihoodUtils.reformatString(_, conf.removeNonAlpha.apply()).split("\\W+") ).
      par.
      map(w => w.toLowerCase).
      par.
      groupBy(w => w).
      par.
      mapValues(_.size)

    println(s"LogLikelihood - Term frequency analytic complete for ${conf.analyticName.apply()}.")
    
    var out = new LogLikelihoodOutput
    out.totalLogLines = Source.fromFile(conf.inputDirectory.apply(), "UTF-8").getLines().length
    out.totalWords = wordCount.map(_._2).reduceLeft(_+_)
    
    println(s"LogLikelihood - Total log line calculation complete for ${conf.analyticName.apply()}.")
    println(s"LogLikelihood - Total log lines for ${conf.analyticName.apply()} = ${out.totalLogLines}")
    println(s"LogLikelihood - Total word count for ${conf.analyticName.apply()} = ${out.totalWords}")
 
    processLines(conf, wordCount, out)
    out
  }

  implicit val ec = ExecutionContext.global

  /**
   * Processes all LogLikelihoodLine objects and populates the LogLikelihoodOutput.lines attribute. This method
   * delegates to LoglikelihoodUtils.
   */
  protected def processLines(conf : LogLikelihoodConf, wordCount : ParMap[String, Int], output : LogLikelihoodOutput) = {
    output.lines = Source.fromFile(conf.inputDirectory.apply(), "UTF-8").getLines().
      map( in => Future{ LoglikelihoodUtils.processLine(in, conf, wordCount, output) })
  }
}

object LogLikelihood {
  val DEC_FORMAT = new DecimalFormat("#.####")
}
