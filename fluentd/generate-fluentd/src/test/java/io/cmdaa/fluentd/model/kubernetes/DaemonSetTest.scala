package io.cmdaa.fluentd.model.kubernetes

import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.scalatest.junit.JUnitRunner

import scala.io.Source
import cats.syntax.either._
import io.circe._
import io.circe.generic.auto._
import io.circe.parser
import io.circe.generic.semiauto
import io.circe.yaml
import io.cmdaa.fluentd.model.kubernetes.DaemonSet.{ConfigMap, DaemonSet, HostPath, Item, Volume, VolumeMount}
import cats.syntax.either._
import io.circe
import io.circe.yaml._
import io.circe.yaml.syntax._
import io.circe.syntax._
import io.circe.Printer

@RunWith(classOf[JUnitRunner])
class DaemonSetTest extends FlatSpec with BeforeAndAfter {

  //    implicit val fooDecoder: Decoder[DaemonSet] = semiauto.deriveDecoder
  //    implicit val fooEncoder: Encoder[DaemonSet] = semiauto.deriveEncoder

  "daemonset" should "parse" in {
//        implicit val fooDecoder: Decoder[DaemonSet] = semiauto.deriveDecoder
//        implicit val fooEncoder: Encoder[DaemonSet] = semiauto.deriveEncoder

    val json = yaml.parser.parse("""
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-es
  namespace: kube-system
  labels:
    k8s-app: fluentd-logging
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
    spec:
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      nodeSelector:
          nodeType: foo
      containers:
        - name: fluentd-elasticsearch
          image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
          resources:
            limits:
              memory: 200Mi
            requests:
              cpu: 100m
              memory: 200Mi
          volumeMounts:
            - name: varlog
              mountPath: /var/log
            - name: varlibdockercontainers
              mountPath: /var/lib/docker/containers
              readOnly: true
      terminationGracePeriodSeconds: 30
      volumes:
        - name: varlog
          hostPath:
            path: /var/log
        - name: varlibdockercontainers
          hostPath:
            path: /var/lib/docker/containers
""")

    val foo = json
      .leftMap(err => err: Error)
      .flatMap(_.as[DaemonSet.DaemonSet])
      .valueOr(throw _)

    println("*******************************")
    println(s"image = ${foo.spec.template.spec.containers.head.image}")
    println(s"name = ${foo.spec.template.spec.containers.head.name}")
    println(s"resources limits memory = ${foo.spec.template.spec.containers.head.resources.limits.memory}")
    println(s"resources requests cpu = ${foo.spec.template.spec.containers.head.resources.requests.cpu}")
    println(s"resources requests memory = ${foo.spec.template.spec.containers.head.resources.requests.memory}")
    println(s"volume mountPath = ${foo.spec.template.spec.containers.head.volumeMounts.head.mountPath}")
    println(s"volume name = ${foo.spec.template.spec.containers.head.volumeMounts.head.name}")
    println("*******************************")
  }

  "daemonset file" should "parse the file" in {
//    implicit val fooDecoder: Decoder[DaemonSet] = semiauto.deriveDecoder
//    implicit val fooEncoder: Encoder[DaemonSet] = semiauto.deriveEncoder
//    val fooDecoder: Decoder[DaemonSet] = semiauto.deriveDecoder
//    val fooEncoder: Encoder[DaemonSet] = semiauto.deriveEncoder

//    implicit val customPrinter = Printer.spaces2.copy(dropNullValues = true)

    val fileLoc = "/home/matt/temp/workflow/fluentd/generate-fluentd/src/test/resources/daemonset.yaml"

    val fileContents = Source.fromFile(fileLoc).getLines.mkString("\n")
    val json = yaml.parser.parse(fileContents)

    val foo = json
      .leftMap(err => err: Error)
      .flatMap(_.as[DaemonSet.DaemonSet])
      .valueOr(throw _)

    //    assert( foo.spec.template.spec.containers.head.image == "quay.io/fluentd_elasticsearch/fluentd:v2.5.2" )
    assert( foo.spec.template.spec.containers.head.image == "fluentd:latest" )
    assert( foo.spec.template.spec.containers.head.name == "fluentd-elasticsearch" )
    assert( foo.spec.template.spec.containers.head.resources.limits.memory == "200Mi" )
    assert( foo.spec.template.spec.containers.head.resources.requests.cpu == "100m" )
    assert( foo.spec.template.spec.containers.head.resources.requests.memory == "200Mi" )
    assert( foo.spec.template.spec.containers.head.volumeMounts.head.mountPath == "/var/log" )
    assert( foo.spec.template.spec.containers.head.volumeMounts.head.name == "varlog" )

    println("*******************************")
    println("*******************************")
    println( foo.asJson.dropNullValues.asYaml.spaces2.replaceAll(".*null\\n","") )

    println("*******************************")
    println("*******************************")

//    foo.spec.template.spec.containers.head.volumeMounts = foo.spec.template.spec.containers.head.volumeMounts ::: List( VolumeMount("matt-test", "/var/log/matt.log", Some( true ) ) )
//    foo.spec.template.spec.volumes = foo.spec.template.spec.volumes ::: List(Volume("matts-log", HostPath("/var/log/matts.log")))

    foo.spec.template.spec.containers.head.volumeMounts =  List( VolumeMount("testfile", "/root/input.log", None, Some( true ) ),
                                                                 VolumeMount("fluentconf", "/fluentd/etc/fluent.conf", None, Some( true ) ),
                                                                 VolumeMount("config-volume", "/etc/config", None, Some( true ) ) )
//    foo.spec.template.spec.volumes = /*foo.spec.template.spec.volumes :::*/ List( Volume( "testfile", Some( HostPath( "/Users/mldonn2/Desktop/wf-fluentd-kafka/input.log", Some("File") ) ), None ),
//                                                                                  Volume( "fluentconf", Some( HostPath( "/Users/mldonn2/Desktop/wf-fluentd-kafka/fluentd.conf", Some("File") ) ), None ) )
    foo.spec.template.spec.volumes =  List( Volume( "testfile", Some( HostPath( "/Users/mldonn2/Desktop/wf-fluentd-kafka/input.log", Some("File") ) ), None ),
                                            Volume( "fluentconf", Some( HostPath( "/Users/mldonn2/Desktop/wf-fluentd-kafka/fluentd.conf", Some("File") ) ), None ),
                                            Volume( "config-volume", None, Some( ConfigMap( "special-config", Some( List( Item( "SPECIAL_LEVEL", "keys" ) ) ) ) ) ) )

    println( foo.asJson.dropNullValues.asYaml.spaces2.replaceAll(".*null\\n","") )
  }
}
