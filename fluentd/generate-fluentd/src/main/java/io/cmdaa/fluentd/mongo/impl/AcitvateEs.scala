package io.cmdaa.fluentd.mongo.impl

import java.io.{File, PrintWriter}
import java.util.UUID

import io.cmdaa.gs.dao.{CmdaaLogDao, MongoComponents}
import io.cmdaa.gs.model.{CmdaaLog, GrokRun, RunStatus}
import reactivemongo.api.bson.BSONDateTime
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class AcitvateEs {

  case class MongDBConnection(val asyncDriver: AsyncDriver, val connection: MongoConnection, val database: Future[DB]) extends MongoComponents {}

  def fromUri(mongoUri: String): Future[MongDBConnection] = {
    val driver = new AsyncDriver

    for {
      uri <- MongoConnection.fromString(mongoUri)
      con <- driver.connect(uri)
    } yield MongDBConnection(driver, con, con.database(uri.db.get))
  }

  def run(efkInstance: Option[String], logId: String, runId: String): Unit = {

    val mongoUri = scala.util.Properties.envOrElse("DATABASE_URL", "mongodb://cmdaa:cmdaa@mongodb:27017/cmdaa")
    val f_mongoComponents = fromUri(mongoUri)

    val testT1 = f_mongoComponents.map(c => new CmdaaLogDao(c)).
      flatMap {
        dao => dao.findByLogId( logId )
      }

    val testT1Result = Await.result(testT1, Duration.Inf).get

    println(s"bucket = ${testT1Result.bucket}")
    println(s"path = ${testT1Result.path}")
    println(s"owner = ${testT1Result.owner}")
    println(s"createdBy = ${testT1Result.meta.createdBy}")
    println(s"runs size = ${testT1Result.runs.size}")

    testT1Result.runs.foreach( in => {
      println(s"id = ${in._id.stringify}")
      println(s"groks size = ${in.groks.size}")
      println(s"passed in id = ${runId}")
      println(s"boolean equals ${in._id.stringify.equals(runId)}")
    })

    val runRes = testT1Result.runs.filter(_._id.stringify.equals(runId))

    def getGrokRun(grokRun: GrokRun) = {
      if( grokRun._id.stringify.equals( runId ) ){

//        val grokRun = GrokRun( BSONDateTime( syncTime ),
//          RunStatus( true, false, false, "Inserting grok runs." ),
//          conf.likelihoodMax.get.get.floatValue(),
//          conf.cosineSim.get.get.floatValue(),
//          groks = grokList.toList )

        GrokRun( grokRun.submitted, grokRun.status, grokRun.logLikelihood, grokRun.cosSimilarity, efkInstance, grokRun._id, grokRun.groks)
//        GrokRun(grokRun.logLikelihood, grokRun.cosSimilarity, efkInstance, grokRun._id, grokRun.groks)
      }else{
        grokRun
      }
    }

    val updatedRuns = testT1Result.runs.map( getGrokRun )
    val updatedLog = CmdaaLog(testT1Result.meta, testT1Result.owner, testT1Result.bucket, testT1Result.path, updatedRuns, testT1Result._id)

    val updateQuery = f_mongoComponents.map(c => new CmdaaLogDao(c)).
      flatMap {
        dao => dao.update( updatedLog )
      }

    val res = Await.result(updateQuery, Duration.Inf)

    println(s"update return code = ${res.code}")
  }
}


object AcitvateEs {
  def main(args: Array[String]): Unit = {
    try {
      val efkInstance = Option( scala.util.Properties.envOrElse("EFK_INSTANCE", "") ).filter( _.nonEmpty )
      val runId = scala.util.Properties.envOrElse("RUN_ID", "")
      val logId = scala.util.Properties.envOrElse("LOG_ID", "")

      println(s"efkInstance = ${efkInstance.getOrElse("")}\nlogId = ${logId}\nrunId = ${runId}\n")

      new AcitvateEs().run( efkInstance, logId, runId )

      val currentGroup = Thread.currentThread.getThreadGroup
      val noThreads = currentGroup.activeCount
      val lstThreads = new Array[Thread](noThreads)
      currentGroup.enumerate(lstThreads)

      try {
        for (i <- 0 until noThreads) {
          System.out.println("Thread No:" + i + " = " + lstThreads(i).getName)

          if (!lstThreads(i).getName.equalsIgnoreCase("main")) {
            lstThreads(i).interrupt()
          }
        }
      } catch {
        case e: Exception => e.printStackTrace()
      }

      System.exit(0)

    } catch {
      case e: Exception => {
        e.printStackTrace()
      }
    }
  }
}