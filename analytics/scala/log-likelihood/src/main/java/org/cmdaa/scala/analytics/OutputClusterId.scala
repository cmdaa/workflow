package org.cmdaa.scala.analytics

import java.io.{File, PrintWriter}
import com.google.gson.GsonBuilder
import org.cmdaa.pojos.LogLikelihoodLine
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf

import scala.collection.mutable
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.{Duration, DurationInt}
import scala.io.Source

/**
  * Created on 3/6/18.
  */
class OutputClusterIdConf( arguments: Seq[String] ) extends LogLikelihoodConf( arguments ){
  val hashes = opt[String](required = true)
}

object OutputClusterId {
  var mappedValues = new mutable.HashMap[Int, LogLikelihoodLine]()
  var conf: OutputClusterIdConf = _

  def run(args: Array[String]): Iterator[Future[LogLikelihoodLine]] = {
    conf = new OutputClusterIdConf(args)

    var logHashes = mutable.Map[String, String]()
    Source.
      fromFile(conf.hashes.apply(), "UTF-8").
      getLines().
      foreach(in => {
        val sp = in.split("\\t")
        if (sp.length == 2) {
          logHashes.put(sp(0).trim, sp(1).trim)
        }
      })

    val llResults = new LogLikelihood().run(conf)
    println(s"Log likelihood analytic complete for ${conf.analyticName.apply()}.")

    implicit val ec = ExecutionContext.global

    println(s"size = ${logHashes.size}")
    val results = llResults.lines.map(in => {
      val ret = Await.result(in, Duration.Inf)
      ret.clusterId = logHashes.get(ret.hash).get
      Future{ ret }
    })

    results
  }

  def main(args: Array[String]): Unit = {
    val log = OutputClusterId
    val output = log.run(args)

    val gson = new GsonBuilder().setPrettyPrinting().create()

    val pw = new PrintWriter(new File(s"${conf.outputDirectory.apply()}/${conf.analyticName.apply()}.out"))

    output.foreach(in => {
      pw.write(gson.toJson(in) + "\n")
    })

    pw.close()
  }
}