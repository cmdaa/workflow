package org.cmdaa.scala.analytics.conf

import org.junit.Assert.{assertEquals, assertFalse, assertTrue}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * Unit tests for LogLikelihoodConf that confirm the following:
 * 1. Correct and complete configuration with long-form CLI args
 * 2. Correct and complete configuration with short-form CLI args
 * 3. remove-non-alpha is False
 * 4. Incomplete configuration with IllegalStateException
 * 5. Incorrect configuration parameter with IllegalStateException
 */
@RunWith(classOf[JUnitRunner])
class LogLikelihoodConfTest extends FunSuite {

  protected var longFormArgs: List[String] = List[String]("--analytic-name",
    "test-analytic", "--input-directory", "/opt/cdma/input", "--output-directory",
    "/opt/cdma/output", "--likelihood-max", "0.50", "--remove-non-alpha")

  protected var shortFormArgs: List[String] = List[String]("-a", "test-analytic",
    "-i", "/opt/cdma/input", "-o", "/opt/cdma/output", "-l", "0.50", "-r")

  protected var noRemoveNonAlphaArgs: List[String] = List[String]("-a", "test-analytic",
    "-i", "/opt/cdma/input", "-o", "/opt/cdma/output", "-l", "0.50")

  protected var incompleteArgs: List[String] = List[String]("--analytic-name",
    "test-analytic", "--input-directory", "/opt/cdma/input", "--output-directory",
    "/opt/cdma/output")

  protected var invalidParamArgs: List[String] = List[String]("--analytic-name",
    "test-analytic", "--input-directory", "/opt/cdma/input",
    "--output-directory", "/opt/cdma/output", "--likelihood-max", "null")

  protected var nonConf: LogLikelihoodConf = _

  test("test long form conf") {
    val conf = new LogLikelihoodConf(longFormArgs)

    assertEquals("test-analytic", conf.analyticName.apply())
    assertEquals("/opt/cdma/input", conf.inputDirectory.apply())
    assertEquals("/opt/cdma/output", conf.outputDirectory.apply())
    assertEquals(0.50, conf.likelihoodMax.apply(), 0.0)
    assertTrue(conf.removeNonAlpha.apply())

    assertTrue(conf.verified)
  }

  test("test short form conf") {
    val conf = new LogLikelihoodConf(shortFormArgs)

    assertEquals("test-analytic", conf.analyticName.apply())
    assertEquals("/opt/cdma/input", conf.inputDirectory.apply())
    assertEquals("/opt/cdma/output", conf.outputDirectory.apply())
    assertEquals(0.50, conf.likelihoodMax.apply(), 0.0)
    assertTrue(conf.removeNonAlpha.apply())

    assertTrue(conf.verified)
  }

  test("test no remove non-alpha conf") {
    val conf = new LogLikelihoodConf(noRemoveNonAlphaArgs)

    assertEquals("test-analytic", conf.analyticName.apply())
    assertEquals("/opt/cdma/input", conf.inputDirectory.apply())
    assertEquals("/opt/cdma/output", conf.outputDirectory.apply())
    assertEquals(0.50, conf.likelihoodMax.apply(), 0.0)
    assertFalse(conf.removeNonAlpha.apply())

    assertTrue(conf.verified)
  }

  test("test incomplete args") {
    val thrown = intercept[IllegalStateException] {
      new LogLikelihoodConf(incompleteArgs)
    }

    assertTrue(thrown.isInstanceOf[IllegalStateException])
    assertEquals("org.rogach.scallop.exceptions.RequiredOptionNotFound: Required option 'likelihood-max' not found"
      , thrown.getMessage)
  }

  test("test invalid param args") {
    val thrown = intercept[IllegalStateException] {
      new LogLikelihoodConf(invalidParamArgs)
    }

    assertTrue(thrown.isInstanceOf[IllegalStateException])
    assertEquals("org.rogach.scallop.exceptions.WrongOptionFormat: Bad arguments for option 'likelihood-max': 'null' "
      + "- wrong arguments format", thrown.getMessage)
  }
}