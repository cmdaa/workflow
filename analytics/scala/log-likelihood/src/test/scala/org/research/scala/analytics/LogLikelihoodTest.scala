package org.cmdaa.scala.analytics

import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created on 2/1/18.
  */
@RunWith(classOf[JUnitRunner])
class   LogLikelihoodTest extends FunSuite {
//  test("test run removeNonAlpha is true") {
//    val conf = new LogLikelihoodConf(List("-i", "src/test/resources/log-likelihood.txt",
//      "-o", "src/test/resources/out.txt", "-l", "0.67", "-a", "llh test removeNonAlpha=true", "-r"))
//    val output = new LogLikelihood().run(conf)
//
//    assertEquals(795, output.lines.size)
//    assertEquals(795, output.totalLogLines)
//    assertEquals(0, output.totalMaxLikelihood)
//    assertEquals(0, output.totalNaN)
//    assertEquals(11493, output.totalWords)
//  }
//
//  test("test run removeNonAlpha is false") {
//    val conf = new LogLikelihoodConf(List("-i", "src/test/resources/log-likelihood.txt",
//      "-o", "src/test/resources/out.txt", "-l", "2", "-a", "llh test removeNonAlpha=false"))
//    val output = new LogLikelihood().run(conf)
//
//    assertEquals(795, output.lines.size)
//    assertEquals(795, output.totalLogLines)
//    assertEquals(0, output.totalMaxLikelihood)
//    assertEquals(0, output.totalNaN)
//    assertEquals(12517, output.totalWords)
//  }
//
//  test("test run removeNonAlpha is false on sample-messages") {
//    val conf = new LogLikelihoodConf(List("-i", "src/test/resources/sample-messages",
//      "-o", "src/test/resources/out.txt", "-l", "2", "-a", "llh test"))
//    val output = new LogLikelihood().run(conf)
//
//    output.lines.size
//  }
}
