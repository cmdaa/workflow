package org.research.date.service

import org.cmdaa.utils.grok.GrokDetector
import org.cmdaa.utils.subsequence.LongestCommonSubsequence
import org.scalatest.FlatSpec

import scala.collection.mutable.ListBuffer

class NodeGrokExtractorLCSTest extends FlatSpec {

  val currentDirectory = new java.io.File(".").getCanonicalPath
  val grokDetector = new GrokDetector(s"${currentDirectory}/src/main/resources/")

  val dateGroks = s"${currentDirectory}/src/main/resources/dates.grok"
  val javaGroks = s"${currentDirectory}/src/main/resources/java.grok"
  val numberGroks = s"${currentDirectory}/src/main/resources/numbers.grok"

  "log level" should "hit one log level" in {
    var groks = ListBuffer[String]()

    var str = "nova-api.log.1.2017-05-16_13:53:08 2017-05-16 00:10:51.513 25746 INFO nova.osapi_compute.wsgi.server [req-ddbf9283-3fb6-4ea4-b19f-8d50d72ad8a1 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] 10.11.10.1 \\\"GET /v2/54fadb412c4e40cdbaed9335e4c35a9e/servers/a015cf14-84bb-4156-a48d-7c4824ac7a9d HTTP/1.1\\\" status: 200 len: 1708 time: 0.1869872"

    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")


    val str1 = grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str1
    println( str1 )

    str = "nova-api.log.1.2017-05-16_13:53:08 2017-05-16 00:11:19.616 25746 INFO nova.osapi_compute.wsgi.server [req-5e0f9d3c-be64-4960-a107-d406900e0ea8 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] 10.11.10.1 \\\"DELETE /v2/54fadb412c4e40cdbaed9335e4c35a9e/servers/a015cf14-84bb-4156-a48d-7c4824ac7a9d HTTP/1.1\\\" status: 204 len: 203 time: 0.3042688"

    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str2 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str2
    println( str2 )

    var l = LongestCommonSubsequence

    var ret = ""

    if( !groks.isEmpty ){
      ret = groks(0)
      groks.foreach( in => {
        ret = l.find( ret, in)
      })
    }

    var tokens = ret.split(" ")

    var rebuilt = ListBuffer[String]()

    str1.split(" ").foreach( in => {
//      println(in)
      if( ret.contains( in ) ){
        rebuilt += in
      } else if ( in.startsWith(".*")) {
        rebuilt += ".*"
      }
    })

    println("RETURN:")
    println(s"\t${compress(rebuilt.toList).mkString(" ")}")
  }

  "instance successfully" should "hit one log level" in {
    var groks = ListBuffer[String]()

    var str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:07:45.253 2931 INFO nova.virt.libvirt.driver [-] [instance: d54b44eb-2d1a-4aa2-ba6b-074d35f8f12c] Instance spawned successfully."
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str1 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str1
    println( str1 )

    str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:07:53.772 2931 INFO nova.virt.libvirt.driver [-] [instance: d54b44eb-2d1a-4aa2-ba6b-074d35f8f12c] Instance destroyed successfully."
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str2 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str2
    println( str2 )

    var l = LongestCommonSubsequence
    var ret = ""

    if( !groks.isEmpty ){
      ret = groks(0)
      groks.foreach( in => {
        ret = l.find( ret, in)
      })
    }

    var tokens = ret.split(" ")

    var rebuilt = ListBuffer[String]()

    str1.split(" ").foreach( in => {
      //      println(in)
      if( ret.contains( in ) ){
        rebuilt += in
      } else  {
        rebuilt += ".*"
      }
    })

    println("RETURN:")
    println(s"\t${rebuilt.mkString(" ")}")
  }

  "claims" should "hit one log level" in {
    var groks = ListBuffer[String]()

    var str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:02:35.076 2931 INFO nova.compute.claims [req-d38f479d-9bb9-4276-9688-52607e8fd350 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] [instance: 95960536-049b-41f6-9049-05fc479b6a7c] Total vcpu: 16 VCPU, used: 0.00 VCPU"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str1 = grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str1
    println( str1 )

    str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:02:35.074 2931 INFO nova.compute.claims [req-d38f479d-9bb9-4276-9688-52607e8fd350 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] [instance: 95960536-049b-41f6-9049-05fc479b6a7c] Total disk: 15 GB, used: 0.00 GB"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str2 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str2
    println( str2 )

    var l = LongestCommonSubsequence
    var ret = ""

    if( !groks.isEmpty ){
      ret = groks(0)
      groks.foreach( in => {
        ret = l.find( ret, in)
      })
    }

    var tokens = ret.split(" ")

    var rebuilt = ListBuffer[String]()

    str1.split(" ").foreach( in => {
      //      println(in)
      if( ret.contains( in ) ){
        rebuilt += in
      } else  {
        rebuilt += ".*"
      }
    })

    println("RETURN:")
    println(s"\t${compress(rebuilt.toList).mkString(" ")}")
  }

  "hibernate" should "hit one log level" in {
    var groks = ListBuffer[String]()

    var str = "Jul  6 03:12:38 calvisitor-10-105-160-37 kernel[0]: hibernate_alloc_pages act 107794, inact 10088, anon 460, throt 0, spec 58021, wire 572831, wireinit 39927"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str1 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str1
    println( str1 )

    str = "Jul  8 03:12:46 calvisitor-10-105-162-175 kernel[0]: did discard act 12083 inact 17640 purgeable 20264 spec 28635 cleaned 0"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str2 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str2
    println( str2 )

    str = "Jul  1 19:43:22 calvisitor-10-105-160-95 kernel[0]: pages 1471315, wire 491253, act 449877, inact 3, cleaned 0 spec 5, zf 23, throt 0, compr 345876, xpmapped 40000"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str3 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str3
    println( str3 )

    str = "Jul  5 20:16:55 calvisitor-10-105-160-210 kernel[0]: could discard act 242131 inact 107014 purgeable 254830 spec 128285 cleaned 0"
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str4 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str4
    println( str4 )

    var l = LongestCommonSubsequence
    var ret = ""

    if( !groks.isEmpty ){
      ret = groks(0)
      groks.foreach( in => {
        ret = l.find( ret, in)
      })
    }

    var tokens = ret.split(" ")

    var rebuilt = ListBuffer[String]()

    str1.split(" ").foreach( in => {
      //      println(in)
      if( ret.contains( in ) ){
        rebuilt += in
      } else  {
        rebuilt += ".*"
      }
    })

    println("RETURN:")
    println(s"\t${compress(rebuilt.toList).mkString(" ")}")
  }

  "build instance" should "hit one log level" in {
    var groks = ListBuffer[String]()

    var str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:09:08.188 2931 INFO nova.compute.manager [req-98474cd9-61e1-4afe-bd52-676a577b058f 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] [instance: 70c1714b-c11b-4c88-b300-239afe1f5ff8] Took 20.83 seconds to build instance."
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str1 = grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str1
    println( str1 )

    str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:09:08.054 2931 INFO nova.compute.manager [req-98474cd9-61e1-4afe-bd52-676a577b058f 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] [instance: 70c1714b-c11b-4c88-b300-239afe1f5ff8] Took 20.05 seconds to spawn the instance on the hypervisor."
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str2 = grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str2
    println( str2 )

    str = "nova-compute.log.1.2017-05-16_13:55:31 2017-05-16 00:09:16.878 2931 INFO nova.compute.manager [req-4df3e4ef-09e7-4806-a7e9-bbb5a052ab1b 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] [instance: 70c1714b-c11b-4c88-b300-239afe1f5ff8] Took 0.99 seconds to destroy the instance on the hypervisor."
    grokDetector.getFormatted(  grokDetector.detectAllGroks(str) ).split(" ").count(_ == s"TIMESTAMP_ISO8601")

    val str3 =  grokDetector.getFormatted( grokDetector.detectAllGroks( str ) )
    groks += str3
    println( str3 )


    var l = LongestCommonSubsequence
    var ret = ""

    if( !groks.isEmpty ){
      ret = groks(0)
      groks.foreach( in => {
        ret = l.find( ret, in)
      })
    }

    var tokens = ret.split(" ")

    var rebuilt = ListBuffer[String]()

    str1.split(" ").foreach( in => {
      //      println(in)
      if( ret.contains( in ) ){
        rebuilt += in
      } else  {
        rebuilt += ".*"
      }
    })

    println("RETURN:")
    println(s"\t${compress(rebuilt.toList).mkString(" ")}")
  }

  def compress[A](l: List[A]):List[A] = l.foldRight(List[A]()) {
    case (e, ls) if (ls.isEmpty || ls.head != e) => e::ls
    case (e, ls) => ls
  }
}