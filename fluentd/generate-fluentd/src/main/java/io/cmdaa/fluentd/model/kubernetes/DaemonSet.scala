package io.cmdaa.fluentd.model.kubernetes

import io.circe.{Decoder, Encoder, Error, Printer, parser, yaml}

import io.circe.generic.semiauto
import io.circe.yaml
import cats.syntax.either._
import io.circe.yaml.syntax._
import io.circe.syntax._
import io.circe.Printer
import io.circe.generic.auto._





object DaemonSet {

//apiVersion: apps/v1
//kind: DaemonSet
//metadata:
//  name: fluentd-es
//  namespace: kube-system
//  labels:
//    k8s-app: fluentd-logging
//spec:
//  selector:
//    matchLabels:
//      name: fluentd-elasticsearch
//  template:
//    metadata:
//      labels:
//        name: fluentd-elasticsearch
//    spec:
//      tolerations:
//        - key: node-role.kubernetes.io/master
//          effect: NoSchedule
//      containers:
//        - name: fluentd-elasticsearch
//          image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
//          resources:
//            limits:
//              memory: 200Mi
//            requests:
//              cpu: 100m
//              memory: 200Mi
//          volumeMounts:
//            - name: varlog
//              mountPath: /var/log
//            - name: varlibdockercontainers
//              mountPath: /var/lib/docker/containers
//              readOnly: true
//      terminationGracePeriodSeconds: 30
//      volumes:
//        - name: varlog
//          hostPath:
//            path: /var/log
//        - name: varlibdockercontainers
//          hostPath:
//            path: /var/lib/docker/containers
//    implicit val fooDecoder: Decoder[DaemonSet] = semiauto.deriveDecoder
//    implicit val fooEncoder: Encoder[DaemonSet] = semiauto.deriveEncoder

    case class DaemonSet(var apiVersion: String,
                         var kind: String,
                         var metadata: MetaData,
                         spec: Spec)
    case class MetaData(var name: Option[String], var namespace: Option[String], var labels: Map[String, String])
    case class Spec(selector: SpecSelector, template: SpecTemplate)
    case class SpecSelector(var matchLabels: Map[String, String])
    case class SpecTemplate(metadata: MetaData, spec: TemplateSpec)

    case class TemplateSpec(var tolerations: Option[ List[TemplateSpecToleration] ] = None,
                            var nodeSelector: Option[ Map[String, String] ] = None,
                            var containers: List[TemplateContainer],
                            var terminationGracePeriodSeconds: Option[Int],
                            var volumes: List[Volume])

    case class TemplateSpecToleration(key: String,
                                      effect: String)
    case class TemplateContainer(var name: String,
                                 var image: String,
                                 var env:  Option[ List[Env] ] = None,
                                 var securityContext: Option[ SecurityContext ],
                                 resources: Resources,
                                 var volumeMounts: List[VolumeMount])
    case class Env(var name: String, var value: Option[String] = None, var valueFrom: Option[ValueFrom] = None)
    case class ValueFrom(fieldRef: FieldRef)
    case class FieldRef(fieldPath: String)

    case class SecurityContext(privileged: Boolean)
    case class Resources(limits: Limits, requests: Requests)
    case class Limits(var memory: String)
    case class Requests(var cpu: String, var memory: String)
    case class VolumeMount(var name: String,
                           var mountPath: String,
                           var subPath: Option[String] = None,
                           var readOnly: Option[Boolean] = Some(false))

    case class Volume(var name: String,
                      hostPath: Option[ HostPath ],
                      configMap: Option[ ConfigMap ])
    case class HostPath(var path: String, var `type`: Option[String])
    case class ConfigMap(var name: String, var items: Option[ List[Item] ])
    case class Item(var key: String, var path: String)


    def fromYaml(yml: String): DaemonSet = {
        val json = yaml.parser.parse(yml)
        val daemonset = json
          .leftMap(err => err: Error)
          .flatMap(_.as[DaemonSet])
          .valueOr(throw _)
        daemonset
    }

    def toYaml(daemonSet: DaemonSet): String = {
        daemonSet.asJson.dropNullValues.asYaml.spaces2.replaceAll(".*null\\n","")

    }

    def fromJson(json: String): DaemonSet = {null}
    def toJson(daemonSet: DaemonSet): String = {null}
}
