package org.cmdaa.scala.analytics.conf

import org.rogach.scallop.ScallopConf
import org.rogach.scallop.exceptions.Exit
import org.rogach.scallop.exceptions.ScallopException

/**
 * The LogLikelihoodConf provides a means of configuring LogLikelihood with command-
 * line arguments. The long form of the parameters is as follows:
 * --analytic-name
 * --input-directory
 * --output-directory
 * --likelihood-max
 * --remove-non-alpha
 * 
 * The short form is as follows:
 * -a
 * -i
 * -o
 * -l
 * -r 
 * 
 * Created on 2/27/18.
 */
class LogLikelihoodConf( arguments: Seq[String] ) extends ScallopConf( arguments ) {
  val analyticName = opt[String](required = true)
  val inputDirectory = opt[String](required = true)
  val outputDirectory = opt[String](required = true)
  val likelihoodMax = opt[Double](required = true)
  val removeNonAlpha = opt[Boolean](required = true)

  /**
    * Overridden implementation throws an uncaught exception in all cases
    */
  override def onError(e: Throwable): Unit = e match {
    case ScallopException(message) => throw new IllegalStateException(e)

    case e => throw new RuntimeException("Non-CLI exception: ", e)
  }
}