package org.cmdaa.pojos

import java.util

import scala.collection.mutable.ArrayBuffer

/**
  * Created on 1/24/18.
  */
class LogLikelihoodLine {
  var hash: String = ""
  var clusterId: String = ""
  var originalMessage: String = ""
  var shortenedMessage: String = ""
  var terms: Array[Term] = _
  var keyString: String = ""
  var logLikelihoodVals = new util.LinkedHashMap[String, Double]()

  def canEqual(other: Any): Boolean = other.isInstanceOf[LogLikelihoodLine]

  override def equals(other: Any): Boolean = other match {
    case that: LogLikelihoodLine =>
      (that canEqual this) &&
        keyString == that.keyString
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(keyString)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

