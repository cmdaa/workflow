# Install Kubernetes on your Mac

## Getting started 
  1.  This guide will walk you through setting up Kubernetes on your local development environement.
      
  *  Recommended hardware requirements for running the CMDAA applicastion.
      * 2.5 GHz 4 core processor
      * 16GB of memory
      * Greater than 20GB of available disk space

## Mac OSX - Method 1
   1.  [Download Docker...select the stable version which now works with Kubernetes](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
   2.  Click the download link
   3.  Download the image and open it.
   4.  Install [Docker Stable](https://docs.docker.com/docker-for-mac/edge-release-notes/) by doing the following<br>
       ![Install Docker Image](images/docker-image.png)
   5.  Navigate to Applications folder and click on Docker<br>
       ![Nav App Folder](images/NavigateApplications.png)
   6.  Click through install<br>
       ![Click through](images/ClickThroughInstall.png)
   7.  Enter Docker Hub credentials<br>
       ![Docker Creds](images/DockerHubCred.png)
   8.  Open up docker settings in menu bar<br>
       ![Docker Settings](images/OpenDockSet.png)
   9.  Open up preferences<br>
       ![Docker Settings](images/OpenPref.png)
   10. Enable Kubernetes<br>
       ![Enable](images/EnableKube.png)
   11. Install Applications<br>
       ![Install Apps](images/InstallApps.png)
   12. It will install(this takes a minute)<br>
       ![Install](images/Install.png)
   13. Open up preferences<br>
       ![Docker Settings](images/OpenPref.png)
   14. Up the limits of resources for docker/kubernetes<br>
       ![Resources](images/preferences_advanced.png)
   15. Run the following command from your terminal<br>
       ```kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml```
   16. Run the kubernetes proxy<br>
       ```kubectl proxy```
   17. Run the following command at the terminal prompt and copy the results<br>
       ```kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | awk '/^deployment-controller-token-/{print $1}') | awk '$1=="token:"{print $2}'```
   18. [Navigate to Kubernetes Dashboard](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/)
   19. Select the Login with Token<br>
       ![dashboard](images/dashboard.png)
   20. Paste the value selected and copied in step 15.
## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 

