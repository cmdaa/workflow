package org.research.date.service

import org.cmdaa.utils.LoglikelihoodUtils
import org.cmdaa.utils.grok.GrokDetector
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GrokPatternTests extends FlatSpec {

  val currentDirectory = new java.io.File(".").getCanonicalPath
  val grokDetector = new GrokDetector(s"${currentDirectory}/src/main/resources/")

  val dateGroks = s"${currentDirectory}/src/main/resources/dates.grok"
  val javaGroks = s"${currentDirectory}/src/main/resources/java.grok"
  val numberGroks = s"${currentDirectory}/src/main/resources/numbers.grok"

//  val grokCompiler = GrokCompiler.newInstance()
//  grokCompiler.registerDefaultPatterns()
//  grokCompiler.register( Source.fromFile(dateGroks).reader() )
//  grokCompiler.register( Source.fromFile(javaGroks).reader() )

  "begin testing" should "test" in {
    val str = "17/06/09 20:10:58 INFO mapred.SparkHadoopMapRedUtil: attempt_201706092018_0024_m_000161_1156: Committed"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TS_ISO8601_SLASHES"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("DATESTAMP_EVENTLOG"))

    println( LoglikelihoodUtils.reformatString(str, true) )
    println( LoglikelihoodUtils.reformatString(str, false) )
  }

  "log level" should "hit one log level" in {
    val str = "2017-12-14T16:21:52,234Z [tserver.TabletServer] DEBUG: gc ParNew=2,492.52(+0.01) DEBUG secs ConcurrentMarkSweep=8.73(+0.00) secs freemem=28,312,422,088(-21,702,434,936) totalmem=51,271,172,096"

    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("LOGLEVEL"))
  }

  "day in log message" should "hit on DATE" in {
    val str = "2017-12-14T16:21:52,234Z [tserver.TabletServer] DEBUG: gc ParNew=2,492.52(+0.01) secs ConcurrentMarkSweep=8.73(+0.00) secs freemem=28,312,422,088(-21,702,434,936) totalmem=51,271,172,096"

    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("LOGLEVEL"))

    //    assert( new GrokExtractor().getReformattedDateTimeString(str).split(" ").count(_ == s"${DateTimeRegex.TIMESTAMP_ISO8601}") == 1 )
  }

  "accumulo" should "hit on DATE" in {
    val str = "2017-12-14 19:36:25,106 [tserver.TabletServer] DEBUG: gc ParNew=2,492.52(+0.01) secs ConcurrentMarkSweep=8.73(+0.00) secs freemem=28,312,422,088(-21,702,434,936) totalmem=51,271,172,096"

    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("LOGLEVEL"))
  }

  "bricks" should "hit on DATE" in {
    val str = "[2017-12-14 19:36:39.285443] E [MSGID: 115050] [server-rpc-fops.c:178:server_lookup_cbk] 0-my-proj: 10843531: LOOKUP /org (00000000-0000-0000-0000-000000000001/org) ==> (Permission denied) [Permission denied]"

    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("POSINT"))
    assert(ret.contains("HOSTPORT"))
  }

  "canary" should "will hit on DATE:00, since it isn't a well known format" in {
    val str = "17-12-14 19:35:21+00:00"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
  }

  "consul" should "hit twice for DATE" in {
    val str = "Dec 14 19:40:30 my.server consul: 2017-12-14 19:40:30 [DEBUG] manager: Rebalanced 5 servers, next active server is another.server (Addr: tcp/127.0.0.1:8080) (DC: dc1)"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("POSINT"))
  }

  "test-log-1" should "hit on DATE" in {
    val str = "2017-12-14 19:41:26,798 my.server-8080 [FileQueServer] ERROR com.burrito.hadoop.input.HDFSFilePickUpClient - - Got a null WorkBundle from WORKSPACE.WORK_SPACE.INPUT.http://127.0.0.1:8080/WorkSpace"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("HOSTPORT"))
  }

  "test-log-2" should "hit on DATE" in {
    val str = "Thu Dec 14 19:41:52 GMT 2017 my.server:8080 : Poolsize active/idle 0/20 other.server.1:8080 : Poolsize active/idle 3/17 other.server.2:8080 : Poolsize active/idle 2/18 other.server.3:8080 : Poolsize active/idle 0/20 other.server.4:8080 : Poolsize active/idle 0/20 other.server.5:8080 : Poolsize active/idle 3/17 other.server.6:8080 : Poolsize active/idle 0/20 other.server.7:8080 : Poolsize active/idle 3/17 other.server.8:8080 : Poolsize active/idle 3/17 other.server.9:8080 : Poolsize active/idle 1/19 other.server.10:8080 : Poolsize active/idle 2/18 other.server.11:8080 : Poolsize active/idle 2/18 Input Count: 77 200MB InProcess Count: 50 326MB Error Count: 0 0MB"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
    assert(ret.contains("HOSTPORT"))
    assert(ret.contains("POSINT"))
  }

  "cron" should "hit on DATE" in {
    val str = "Dec 14 19:42:03 my.server CROND[235093]: (root) MAIL (mailed 20 bytes of output but got status 0x004b#012)"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
    assert(ret.contains("POSINT"))
  }

  "test-log-4" should "hit on DATE" in {
    val str = "2017-12-14T19:39:29.435+0000 [clientcursormon] connections:83"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("HOSTPORT"))
  }

  "test-log-5" should "hit on DATE" in {
    val str = "[1] 14 Dec 14:43:45.084 * 10000 changes in 60 seconds. Saving..."
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIME"))
    assert(ret.contains("POSINT"))
  }

  "test-log-6" should "hit on DATE" in {
    val str = "2017-12-14 17:54:08,061:1(0x7faa6e13e700):ZOO_WARN@zookeeper_interest@1557: Exceeded deadline by 27ms"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("LOGLEVEL"))
  }

  "test-log-7" should "hit on DATE" in {
    val str = "2017-12-14 19:46:36,176 INFO org.apache.hadoop.hdfs.server.datanode.DataNode.clienttrace: src: 127.0.0.1, dest: 127.0.0.1, op: REQUEST_SHORT_CIRCUIT_FDS, blockid: 6026461116, srvID: 4f5f3266-cf37-40a5-9d01-59066861d33f, success: true"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("IPV4"))
  }

  "test-log-8" should "hit on DATE" in {
    val str = "2017-12-14 19:47:22,571 INFO org.apache.hadoop.yarn.server.nodemanager.containermanager.monitor.ContainersMonitorImpl: Memory usage of ProcessTree 209405 for container-id container_e51_1512508162707_8721_01_000051: 3.2 GB of 9 GB physical memory used; 10.1 GB of 36 GB virtual memory used"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("POSINT"))
  }

  "ipa-server" should "hit on DATE:TIME" in {
    val str = "[14/Dec/2017:19:47:52.734534586 +0000] conn=377197 op=262 SRCH base=\"my log stuff\" scope=2 filter=\"(&(ipServicePort=21572)(ipServiceProtocol=udp)(objectClass=ipService))\" attrs=\"objectClass cn ipServicePort ipServiceProtocol entryusn\""
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("DAY_MONTH_YEAR"))
    assert(ret.contains("TIME_WITH_MS"))
    assert(ret.contains("POSINT"))
  }

  "kafka" should "hit on DATE" in {
    val str = "2017-12-14 19:48:03,363 INFO kafka.log.Log: Rolled new log segment for 'abcd-36' in 1 ms."
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
  }

  "logdrive" should "hit DATETTIME, not well known format" in {
    val str = "2017-12-14T19:40 ERROR: Disk drive errors. host=my.server, state=good, slot=16, deviceId=10, partition=sdl, smartAlert=No, errors=75643, serial=WMC1F0E996PT, host-serial="
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
  }

  "mesos" should "hit on TIME" in {
    val str = "I1214 19:50:44.150166 2552 slave.cpp:5267] Current disk usage 44.04%. Max allowed age: 22.062611754589721hrs"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIME_WITH_MS"))
    assert(ret.contains("POSINT"))
    assert(ret.contains("HOSTPORT"))
  }

  "messages" should "hit on DATE" in {
    val str = "Dec 14 19:51:33 my.server systemd: collectd_exporter.service holdoff time over, scheduling restart."
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
  }

  "nginx-access" should "hit on DATE:TIME, not well known format" in {
    val str = "172.18.7.4 - - [14/Dec/2017:20:06:42 +0000] \"GET /healthcheck HTTP/1.1\" 200 - \"-\" \"my-message/1.3.4\" 1"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("IPV4"))
    assert(ret.contains("DAY_MONTH_YEAR"))
    assert(ret.contains("TIME"))
    assert(ret.contains("POSINT"))
  }

  "test-log-9" should "hit on DATE" in {
    val str = "2017-12-14 20:08:32 GMT INFO, Processing by QueriesController#index as JSON"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
  }

  "nginx-access" should "hit DATE:TIME, not well known format" in {
    val str = "7.169.130.141 - - [14/Dec/2017:20:11:03 +0000] \"GET /\" 400 252 \"-\" \"-\" \"-\""
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("IPV4"))
    assert(ret.contains("DAY_MONTH_YEAR"))
    assert(ret.contains("TIME"))
    assert(ret.contains("POSINT"))
  }

  "spark" should "get iso 8601" in {
    val str = "2017/06/09 20:11:09 INFO executor.Executor: Finished task 39.0 in stage 27.0 (TID 1279). 2267 bytes result sent to driver"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TS_ISO8601_SLASHES"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("POSINT"))
  }

  "nginx-error-slashed-date" should "hit on DATE" in {
    val str = "17/12/14 20:11:34 [error] 116#116: *400257 [lua] marathon.lua:113: balance(): ip address returned by balancer from list provided by marathon: 127.0.0.1:8080 while connecting to upstream, client: 127.0.0.1, server: my.test.server.url, request: \"GET /my/test/url\""
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TS_ISO8601_SLASHES"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("HOSTPORT"))
    assert(ret.contains("IPV4"))
  }

  "nginx-error" should "hit on DATE" in {
    val str = "2017-12-14 20:11:34 [error] 116#116: *400257 [lua] marathon.lua:113: balance(): ip address returned by balancer from list provided by marathon: 127.0.0.1:8080 while connecting to upstream, client: 127.0.0.1, server: my.test.server.url, request: \"GET /my/test/url\""
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("HOSTPORT"))
    assert(ret.contains("IPV4"))
  }

  "nifi-app" should "hit on DATE" in {
    val str = "2017-12-14 20:12:19,459 INFO [Process Pending Heartbeats] myservice.cluster.heartbeat Received heartbeat for node [id=f959627f-250b-4e57-886e-cebcd7edad88, apiAddress=my.test.url, apiPort=8080, socketAddress=my.test.api, socketPort=8080]."
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
  }

  "nifi-user" should "hit on DATE" in {
    val str = "2017-12-14 20:13:06,649 INFO [Web Server-4271218] foo.bar.baz.NodeAuthorizedUserFilter Attempting request for (info) GET https://127.0.0.1/controller (source ip: 127.0.0.1)"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("LOGLEVEL"))
  }

  "salt-minion" should "hit on DATE" in {
    val str = "2017-12-14 20:07:26,962 [salt.utils.schedule][INFO ][255893] Running scheduled job: __mine_interva"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("POSINT"))
  }

  "secure" should "hit on DATE" in {
    val str = "Dec 14 20:14:54 server.url sshd[161961]: pam_unix(sshd:session): session opened for user nifi by (uid=0)"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
    assert(ret.contains("POSINT"))
  }

  "traefik" should "hit on DATE" in {
    val str = "time=\"2017-12-14T16:21:52Z\" level=info msg=\"Skipping same configuration for provider marathon\""
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
  }

  "date time seperate" should "hit on DATE" in {
    val str = "2017-12-14T16:21:52,234Z [tserver.TabletServer] DEBUG: 20:07:26,962 gc ParNew=2,492.52(+0.01) 2017-12-14 secs ConcurrentMarkSweep=8.73(+0.00) secs freemem=28,312,422,088(-21,702,434,936) totalmem=51,271,172,096"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("JAVACLASS"))
    assert(ret.contains("LOGLEVEL"))
  }

  "date not picking up" should "foobar" in {
    val str = "Feb  5 20:33:34 server automount[3286]: expire_proc_indirect: expire /home/randomuser"

    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("SYSLOGTIMESTAMP"))
    assert(ret.contains("POSINT"))
  }

  "num with colon" should "parse num" in {
    val str = "2015-10-18 18:01:53,510 INFO [IPC Server listener on 62270] org.apache.hadoop.ipc.Server: IPC Server listener on 62270: startingr"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("POSINT"))
    assert(ret.contains("JAVACLASS"))
  }

  "test date detactor" should "detect date" in {
    val one = grokDetector.getFormatted(  grokDetector.detectAllGroks("2017-12-14 20:07:26,962 ") )
    val two = grokDetector.getFormatted(  grokDetector.detectAllGroks("17-12-14 20:07:26,962 ") )

    println(one)
    println(two)
  }

  "parse nums" should "should parse nums correctly" in {
    val str = "2015-08-07 07:27:47,650 - INFO  [QuorumPeer[myid=1]/0:0:0:0:0:0:0:0:2181:Follower@63] - FOLLOWING - LEADER ELECTION TOOK - 238"
    val ret = grokDetector.getFormatted(  grokDetector.detectAllGroks(str) )

    assert(ret.contains("TIMESTAMP_ISO8601"))
    assert(ret.contains("LOGLEVEL"))
    assert(ret.contains("HOSTPORT"))
  }

  "parseTest" should "" in {
    val res = LoglikelihoodUtils.reformatString("2017-12-14T16:21:52,234Z [tserver.TabletServer] DEBUG: 20:07:26,962 gc ParNew=2,492.52(+0.01) 2017-12-14 secs ConcurrentMarkSweep=8.73(+0.00) secs freemem=28,312,422,088(-21,702,434,936) totalmem=51,271,172,096", true)
    println(res)
  }
}