package org.cmdaa.scala.analytics

import java.io.{File, PrintWriter}
import com.google.gson.GsonBuilder
import org.joda.time.DateTime
import org.cmdaa.pojos.LogLikelihoodLine
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.utils.{CartesianProduct, Features}

import scala.collection.mutable
import scalax.collection.GraphEdge._
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, Future, future}

/**
  * Created on 1/18/18.
  */
class LogLikelihoodConnectComponentsConf( arguments: Seq[String] ) extends LogLikelihoodConf( arguments ){
  val cosineSim = opt[Double](required = true)
}

object LogLikelihoodConnectComponents {

  var mappedValues = new mutable.HashMap[Int, LogLikelihoodLine]()
  var conf: LogLikelihoodConnectComponentsConf = _

  def run(args: Array[String]): Graph[Int, UnDiEdge] = {
    conf = new LogLikelihoodConnectComponentsConf(args)
    println(s"Beginning analytic run for ${conf.analyticName.apply()}.")
    val llResults = new LogLikelihood().run(conf)
    println(s"Log likelihood analytic complete for ${conf.analyticName.apply()}.")


//    val pool = collection.parallel.ThreadPoolTasks.defaultThreadPool
//    val futures = llResults.lines.map( in => {
//      pool submit new java.util.concurrent.Callable[B] { def call = // A => B }
//    })
////    val futures = llResults.lines.map { a: A =>
////      pool submit new java.util.concurrent.Callable[B] { def call = // A => B }
////    }
//
//    val a = Future.traverse(llResults.lines)( x => Future())
//    println( Await.result(a, Duration.Inf).size )
//
//    val inverseFuture: Future[Matrix] = Future {
//      fatMatrix.inverse() // non-blocking long lasting computation
//    }(executionContext)

    // reduce to hashed keys
    var minimizedLLRes = llResults.lines.toStream.par.groupBy(in =>  Await.result( in, Duration.Inf ).keyString).par.map(_._2.head).iterator

//    var minimizedLLRes = llResults.lines.toStream.par.groupBy(in => in.keyString).par.map(_._2.head).iterator
    println(s"Deleted duplicate hashed keys for ${conf.analyticName.apply()}.")

    var list: Stream[LogLikelihoodLine] = (minimizedLLRes).toStream.map(in => Await.result(in, Duration.Inf))
    val numNodes = list.length
    println(s"Converted keys to stream for ${conf.analyticName.apply()}.")
    println(s"Number of unique log messages for ${conf.analyticName.apply()} = ${numNodes}")

    import java.util.concurrent.ConcurrentHashMap

    println("AAAAAAAAAAAAAA")

//    var map2:ConcurrentHashMap[Int, LogLikelihoodLine] = ConcurrentHashMap[Int, LogLikelihoodLine]

    var map:ConcurrentHashMap[Int, LogLikelihoodLine] = new ConcurrentHashMap[Int, LogLikelihoodLine]()
    println("BBBBBBBBBBBBBB")

    //    var map:mutable.Map[Int, LogLikelihoodLine] = mutable.Map[Int, LogLikelihoodLine]()

    list.par.foreach( in => {
      map.put(in.keyString.hashCode, in)
    })
    println("CCCCCCCCCCCCCC")

    val hashes:List[Int] = list.map( in => {in.keyString.hashCode}).toList


    // do cartesian product and cosine similarity between all of the strings in the set.
    var cartesianProduct = new CartesianProduct[Int]().cross(hashes).par.map(in => {
      if (in._1 < in._2) (in._1, in._2) else (in._2, in._1)
    }).distinct

    println("DDDDDDDDDDDDDD")

    println(s"Cartesian product complete for ${conf.analyticName.apply()}.")

    var sims = cartesianProduct.par.map(in => {
      val similarity = Features.findCosineSimilarity(map.get(in._1).keyString, map.get(in._2).keyString)
      (similarity, in._1, in._2)
    }).par.filter(in => in._1 > conf.cosineSim.apply())



//    // do cartesian product and cosine similarity between all of the strings in the set.
//    var cartesianProduct = CartesianProduct.cross(list).par.map(in => {
//      if (in._1.keyString.hashCode < in._2.keyString.hashCode) (in._1, in._2) else (in._2, in._1)
//    }).distinct
//
//    println(s"Cartesian product complete for ${conf.analyticName.apply()}.")
//
//    var sims = cartesianProduct.par.map(in => {
//      val similarity = Features.findCosineSimilarity(in._1.keyString, in._2.keyString)
//      (similarity, in._1, in._2)
//    }).par.filter(in => in._1 > conf.cosineSim.apply())

    println(s"Cosine similarity complete for ${conf.analyticName.apply()}.")

    var size = sims.toStream.size

    println("**************** SIZE = " + size)

    // NOTE: This has to be input with a list or a stream.  A parallel collection messes it up. #NotThreadSafe #POS
    var g = Graph[Int, UnDiEdge]()

    var count = 0
    val dateTimeFormat = "yyyy-MM-dd HH:mm:ss.SSSZ"
    sims.seq.map(in => {
      g += (map.get(in._2).keyString.hashCode ~ map.get(in._3).keyString.hashCode)
      mappedValues.put(map.get(in._2).keyString.hashCode, map.get(in._2))
      mappedValues.put(map.get(in._3).keyString.hashCode, map.get(in._3))
      count += 1
      if (count % 10000 == 0) println(s"${DateTime.now().toString(dateTimeFormat)} Added ${count} edges")
    })

    println(s"Clustering complete for ${conf.analyticName.apply()}.")
    g
  }

  def main(args: Array[String]): Unit = {
    val log = LogLikelihoodConnectComponents
    val graph = log.run(args)

    var componentTraverser = graph.componentTraverser().toStream
    val gson = new GsonBuilder().setPrettyPrinting().create()

    val pw = new PrintWriter(new File(s"${conf.outputDirectory.apply()}/${conf.analyticName.apply()}.out"))
    val clusters = new PrintWriter(new File(s"${conf.outputDirectory.apply()}/${conf.analyticName.apply()}-clusters.out"))

    println(s"Number of connected components found for ${conf.analyticName.apply()} = ${componentTraverser.size}")
    println(s"Begin writing file for ${conf.analyticName.apply()}.")

    var objCount = 0
    var groupCount = 0
    for (component <- componentTraverser) {
      pw.write("\n-------------------------------------------------------------------------------------------------------\n")
      val root = log.mappedValues.get(component.root.value.productIterator.next().toString.toInt)

      component.nodes.seq.map(in => {
        val node = log.mappedValues.get(in.value.productIterator.next().toString.toInt)
        pw.write(gson.toJson(node.get))
        println(s"${node.get.hash}\t${root.get.hash}\n")
        clusters.write(s"${node.get.hash}\t${root.get.hash}\n")
        objCount += 1
      })
      pw.write("\n-------------------------------------------------------------------------------------------------------\n")
      groupCount += 1
    }
    println(s"End writing file for ${conf.analyticName.apply()}.")

    println(s"Objects written = ${objCount}")
    println(s"Groups written = ${groupCount}")
    pw.close()
    clusters.close()
  }
}