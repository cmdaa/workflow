package org.cmdaa.utils.multiline

import scala.collection.mutable.{ListBuffer, MutableList}

/**
  * Identifies and tracks multi-line message patterns, each of which is composed of
  * an ordered list of message strings.
  *  
  * Created on 3/26/18.
  */
class MultiLineDetector {
  /**
   * List of lists that contain 2..n message strings
   */
  val multiLines = new MutableList[MutableList[String]]

  /**
   * Inserts two, ordered message strings into a log message group.
   */
  def insert(pre: String, post: String) = {
    // Determine which log message group each message appears
    val posPre = getListPos(pre)
    val posPost = getListPos(post)

    if (isNewMessageSet(posPre, posPost)) {
      /*
       * Neither message is within an existing message group. Accordingly, create
       * a new message group (List) and add both messages to it.
       */
      processNewMessageGroup(pre, post)
    } else {
      /*
       * At least one of the messages is in an existing message group, so 
       * process one or both, if applicable
       */
      if (!isNewMessage(posPre) && !isNewMessage(posPost)) {
        /*
         * TODO confirm if this should remain a noop
         * if posPost is the first in the list, then insert posPre at the head of the list
         * if posPre is at the end of a list, insert posPost behind it
         * if either of those conditions arise, then we need to create a new list?
         * var v = new mutable.MutableList[String]
         * v += pre
         * v += post
         * multiLines += v
         * 
         */
        if (posPre == posPost) {
          /*
           * noop, both messages are in the same message group. Accordingly, these 
           * two messages do not correspond to a new message group nor additions
           * to an existing message group
           */
        }
      } else if (!isNewMessage(posPre) && isNewMessage(posPost)) {
        /*
         * While the first message is an existing message group, the second message is
         * not. Retrieve the message group containing the first message and add append 
         * the group with the second message IF the first message is the LAST message
         * in the message group. Otherwise, noop.
         * 
         */
        var list = multiLines(posPre)
        if (list.last == pre) {
          list += post
        }    
      } else if (isNewMessage(posPre) && !isNewMessage(posPost)) {
        /*
         * While the second message is an existing message group, the first message is
         * not. Retrieve the message group containing the second message and add
         * prepend the group with the first message IF the second message is the FIRST 
         * message in the message group. Otherwise, noop.
         * 
         */
        var list = multiLines(posPost)
        if (list.head == post) {
          list.+=:(pre)
        }
      }
    }
  }

  /**
   * Confirms if both message strings do not appear in existing message groups.
   */
  protected def isNewMessageSet(posOne : Int, posTwo : Int) : Boolean = {
    posOne == MultiLineDetector.NULL_POSITION && 
      posTwo == MultiLineDetector.NULL_POSITION
  }
  
  /**
   * Confirms if the message string does not appear in an existing message group.
   */
  protected def isNewMessage(pos : Int) : Boolean = {
    pos == MultiLineDetector.NULL_POSITION
  }
  
  /**
   * Creates new message group and adds both message strings to it
   */
  protected def processNewMessageGroup(pre: String, post : String) = {
    val v = new MutableList[String]
    v += pre
    v += post
    multiLines += v    
  }
  
  /**
   * Determines the index of the message group the string appears. If the string
   * does not belong to any message group, this method returns 
   * MultiLineDetector.NULL_POSITION.
   * 
   * @return index of message group the string appears
   */
  protected[multiline] def getListPos(value: String) : Int = {
    /*
     * If there is at least one message group, check to see which 
     * group the message appears in and return the position of that
     * group in the multiLines object. 
     */
    if (multiLines.size > 0) {      
      multiLines.iterator.zipWithIndex.foreach{ case(messageGroup, i) => 
        { if (messageGroup.contains(value)) return i }
      }
    }
    
    //There are no message groups or none of them contain the value
    return MultiLineDetector.NULL_POSITION
  }

  /**
   * Returns a copy of the multiLines object
   * @return multiLines
   */
  def getLines() : MutableList[MutableList[String]] = { multiLines.clone() }

  /**
   * Generates a List of space-delimited strings corresponding to the message
   * groups encapsulated in the multiLines object
   * @return list of message groups exported as spaced-delimited strings 
   */
  def getMessageKeyList() : List[String] = {
    multiLines.map( _.toList.mkString(" ") ).toList
  }

  /**
   * Iterates through all message keys and returns the size of the 
   * longest message key
   * 
   * @return int that indicates the longest message 
   */
  def getSlidingWindow() : Int = {
    getMessageKeyList().map( in => {
      in.split(" ").size
    } ).max
  }
}

object MultiLineDetector {
  val NULL_POSITION : Int = -1
}
