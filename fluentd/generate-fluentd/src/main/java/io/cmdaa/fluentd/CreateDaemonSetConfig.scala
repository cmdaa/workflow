package io.cmdaa.fluentd

//import io.circe.generic.semiauto
import java.io.PrintWriter
import io.circe.{Decoder, Encoder, Error, Printer, parser, yaml}
import io.circe.yaml.syntax._
import io.circe.syntax._
import io.cmdaa.fluentd.model.kubernetes.FileMapping.{FileMapping, FileMappingConfigMap}
import cats.syntax.either._
import io.cmdaa.fluentd.model.kubernetes
import io.cmdaa.fluentd.model.kubernetes.DaemonSet.{ConfigMap, DaemonSet, Env, HostPath, Item, Volume, VolumeMount}

import scala.io.Source
import io.circe.generic.auto._

import scala.io.Source
import cats.syntax.either._

class CreateDaemonSetConfig {}

object CreateDaemonSetConfig {
  def main(args: Array[String]): Unit = {
    try {
      val LOG_NAME = "LOG_NAME"

      val input = sys.env.getOrElse("INPUT_FILE", "/usr/share/cmdaa/daemonset-template.yaml")
      val output = sys.env.getOrElse("OUTPUT_FILE", "/usr/share/cmdaa/daemonset.yaml")

      val nameSpace = sys.env.getOrElse("NAMESPACE", "kube-system")
      val daemonName = sys.env.getOrElse("DAEMON_NAME", "cmdaa-fluentd")
      val nodeSelector = sys.env.getOrElse("NODE_SELECTOR", "cmdaa")
      val memLimit = sys.env.getOrElse("MEMORY_LIMIT", "200Mi")
      val memRequest = sys.env.getOrElse("MEMORY_REQUEST", "200Mi")
      val cpuRequest = sys.env.getOrElse("CPU_REQUEST", "100m")
      val logName = sys.env.getOrElse(s"${LOG_NAME}", "log")

      println(s"INPUT_FILE = ${input}\nOUTPUT_FILE = ${output}\nNAMESPACE = ${nameSpace}\nDAEMON_NAME = ${daemonName}\nNODE_SELECTOR = ${nodeSelector}\nMEMORY_LIMIT = ${memLimit}\nMEMORY_REQUEST = ${memRequest}\nCPU_REQUEST = ${cpuRequest}\n")

      val volumeMount = sys.env.getOrElse("VOLUME_MOUNT", "[]")
      val configMapMount = sys.env.getOrElse("CONFIG_MAP_MOUNT", "[]")

      println(Source.fromFile(input).getLines.mkString("\n"))

      val daemonset = kubernetes.DaemonSet.fromYaml( Source.fromFile(input).getLines.mkString("\n") )

      val variables = scala.collection.mutable.ListBuffer.empty[Env]

      daemonset.spec.template.spec.containers.head.env match {
        case Some(envVariables) => {
          var logNameFound = false
          envVariables.foreach( in => {
            if( in.name.equalsIgnoreCase(s"${LOG_NAME}") ) {
              variables += Env( s"${LOG_NAME}", Some(logName) )
              logNameFound = true
            }else {
              variables += in
            }
          })
          if( !logNameFound ){
            variables += Env( s"${LOG_NAME}", Some(logName) )
          }
        }
        case None => {
          println("No environment variables defined.  Please check your config.")
        }
      }

      daemonset.spec.template.spec.containers.head.env = Some( variables.toList )
//      envVariables.foreach( in => {
//        if( in.name.equalsIgnoreCase(s"${LOG_NAME}") ) {
//          variables += Env( s"${LOG_NAME}", Some(logName) )
//        }
//        variables += in
//      })

      // daemonName
      daemonset.metadata.name = Some(daemonName)
      daemonset.metadata.labels = Map[String, String]( ("k8s-app" -> daemonName) )
      daemonset.spec.selector.matchLabels = Map[String, String] ( ("name" -> daemonName) )
      daemonset.spec.template.metadata.labels = Map[String, String] ( ("name" -> daemonName) )

      // namespace
      daemonset.metadata.namespace = Some( nameSpace )

      // nodeselector
      daemonset.spec.template.spec.nodeSelector = Some( Map[String, String] ( ("nodeType" -> nodeSelector) ) )

      //memory limit/request
      daemonset.spec.template.spec.containers.head.resources.limits.memory = memLimit
      daemonset.spec.template.spec.containers.head.resources.requests.memory = memRequest

      //cpu request
      daemonset.spec.template.spec.containers.head.resources.requests.cpu = cpuRequest

      val volumes = parser.decode[List[FileMapping]](volumeMount).leftMap(err => err: Error).valueOr(throw _)
      println(volumes)

      val configMapVolumes = parser.decode[List[FileMappingConfigMap]](configMapMount).leftMap(err => err: Error).valueOr(throw _)
      println(configMapVolumes)

      daemonset.spec.template.spec.containers.head.volumeMounts = List[VolumeMount]()
      daemonset.spec.template.spec.volumes = List[Volume]()

      volumes.foreach( in => {

        val mount = VolumeMount( in.name,
                                 in.containerLocation,
                                 None,
                                 Some(in.readOnly) )
        val volume = Volume( in.name,
                             Some( HostPath( in.physicalLocation, Some( in.`type` ) ) ),
                             None )

        println(s"volume = volume(${volume.name}, ${volume.hostPath.get}, ${volume.configMap})")
        daemonset.spec.template.spec.containers.head.volumeMounts = daemonset.spec.template.spec.containers.head.volumeMounts ::: List( mount )
        daemonset.spec.template.spec.volumes = daemonset.spec.template.spec.volumes ::: List( volume )
      })

      configMapVolumes.foreach( in => {
        val mount = VolumeMount( in.name,
                                 in.containerLocation,
                                 None,
                                 Some( in.readOnly ) )

        def getProps(configMapKey: String, path: String) : Option[List[io.cmdaa.fluentd.model.kubernetes.DaemonSet.Item]] = {
          if( configMapKey.equalsIgnoreCase("") && path.equalsIgnoreCase("") ){
            None
          }else {
            Some( List[Item]( Item(configMapKey, path) ) )
          }
        }

        val volume = Volume( in.name,
                             None,
                             Some( ConfigMap( in.configMapName, getProps(in.configMapKey, in.newPath) ) ) )

        println(s"volume config = volume(${volume.name}, ${volume.hostPath}, ${volume.configMap.get.name})")
        daemonset.spec.template.spec.containers.head.volumeMounts = daemonset.spec.template.spec.containers.head.volumeMounts ::: List( mount )
        daemonset.spec.template.spec.volumes = daemonset.spec.template.spec.volumes ::: List( volume )
      })

      val yamlOutput = kubernetes.DaemonSet.toYaml( daemonset )
      println( yamlOutput )
      new PrintWriter(output) { write( yamlOutput ); close }
    }
    catch {
      case e: Exception => {
        e.printStackTrace()
      }
    }
  }
}