package org.cmdaa.scala.analytics

import java.io.{File, PrintWriter}
import com.google.gson.{Gson, GsonBuilder}
import org.cmdaa.pojos.LogLikelihoodLine
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.utils.grok.GrokDetector
import org.cmdaa.utils.subsequence.LongestCommonSubsequence

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}
import scala.io.Source
import scala.util.Random

case class GrokOutFile(grok: String)
case class GrokLogessagesOutFile(logMessage: String)
case class S3OutputOutFile(grok: String, logMessages: java.util.List[GrokLogessagesOutFile])

/**
  * Created on 3/6/18.
  */
class SaveGroksToFileConf( arguments: Seq[String] ) extends LogLikelihoodConf( arguments ){
  val hashes = opt[String](required = true)
//  val s3Id = opt[String](required = true)
  val s3File = opt[String](required = true)
}

object SaveGroksToFile {
  var mappedValues = new mutable.HashMap[Int, LogLikelihoodLine]()
  var conf: SaveGroksToFileConf = _
  val grokDetector = new GrokDetector( scala.util.Properties.envOrElse( "GROK_CONF_DIR","/cmdaa/conf/groks" ) )

    def run(args: Array[String]): Unit = {
      val grokOut = new PrintWriter( new File( scala.util.Properties.envOrElse("OUTPUT_FILE", s"/tmp/grok.out" ) ) )
      conf = new SaveGroksToFileConf(args)

      var logHashes = mutable.Map[String, String]()
      Source.
        fromFile(conf.hashes.apply(), "UTF-8").
        getLines().
        foreach(in => {
          val sp = in.split("\\t")
          if (sp.length == 2) {
            logHashes.put(sp(0).trim, sp(1).trim)
          }
        })

      val llResults = new LogLikelihood().run(conf)

      println(s"Log likelihood analytic complete for ${conf.analyticName.apply()}.")
      println(s"size = ${logHashes.size}")

      val results = llResults.lines.toStream.par.map(f => {
        val in = Await.result(f, Duration.Inf)
        in.clusterId = logHashes.get(in.hash).get
        in
      }).toList

      println("results stream ready.")

      val groupedVals = results.groupBy(in => { in.clusterId })

      println("groups ready.")

      try {
        val foo = groupedVals.par.foreach(in => {

          val logMsgs = ListBuffer[GrokLogessagesOutFile]()
          println("Group: ")
          val ret = in._2.take(10)

          var groks = ListBuffer[String]()

          ret.par.foreach( out => {
            val ffff = grokDetector.getFormatted( grokDetector.detectAllGroks( ret(0).originalMessage ) )
            groks.synchronized{
              groks += ffff
            }
          } )

          println("groks detected")
          var l = LongestCommonSubsequence
          var tempGrok = ""

          if( !groks.isEmpty ){
            tempGrok = groks(0)
            groks.par.foreach( in => {
              l.synchronized {
                tempGrok = l.find(tempGrok, in)
              }
            })
          }
          println("longest common subsequence found.")
          val grok = new GrokOutFile(tempGrok)


          ret.par.foreach(out => {
             logMsgs.synchronized {
               logMsgs += GrokLogessagesOutFile(out.originalMessage)
             }
          })

//          println(s"grok = ${grok}")

          val s3 = new S3OutputOutFile( grok.grok, logMsgs.asJava )
          s3.logMessages.asScala.foreach( in => { println(s"\t${in}") } )

          val gson = new Gson()
          grokOut.write( gson.toJson( s3 ) + "\n" )
          grokOut.close()
          ""
        })
      } catch {
        case exception:Exception => exception.printStackTrace()
      } finally {
        grokOut.close()
      }
    }

  def main(args: Array[String]): Unit = {
    val log = SaveGroksToFile
    log.run(args)
  }
}