package org.cmdaa.utils

/**
 * Consists of utility methods for generating word -> number of occurrences in
 * a single or set of log messages.
 */
object WordUtils {

  /**
    * Generates mappings of word -> number of times word appears in the input string.
    *
    * Important Note: the word count logic is independent of case.
    *
    * @param String
    * @return Map of word -> number of occurrences in the input String
    */
  def generateWordCountsMap(value: String): Map[String, Int] = {
    value.split("\\W+").
      toList.
      map(w => w.toLowerCase).
      groupBy(w => w).
      mapValues(_.size)
  }

  /**
    * Returns a mapping of word -> 1 for all words appearing in a String. This method
    * basically returns a Map containing keys that correspond to all unique words
    * appearing in the input string.
    *
    * Important Note: the word count logic is independent of case.
    *
    * @param String
    * @return Map of word -> 1
    */
  def generateUniqueWordsMap(value: String): java.util.Map[CharSequence, Integer] = {
    var ret: java.util.Map[CharSequence, Integer] = new java.util.HashMap[CharSequence, Integer]()
    for (in <- generateWordCountsMap(value)) {
      ret.put("" + in._1, 1)
    }
    ret
  }
}