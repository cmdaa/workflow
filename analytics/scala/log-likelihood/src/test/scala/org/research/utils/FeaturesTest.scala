package org.cmdaa.utils

import io.krakens.grok.api.GrokCompiler
import org.cmdaa.utils.grok.GrokDetector
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import org.junit.Assert._

import scala.collection.JavaConverters._
import scala.io.Source

/**
  * Created on 1/26/18.
  */
@RunWith(classOf[JUnitRunner])
class FeaturesTest extends FlatSpec {
  "cosine similarity identical strings" should "return 1.0d" in {
    assertEquals(Features.findCosineSimilarity("this is a test", "this is a test"), 1d, 0.0)
  }

  "cosine similarity dissimilar strings" should "return 0.0d" in {
    assertEquals(Features.findCosineSimilarity("this is a test", "string shoudl not be similar"), 0.0d, 0.0)
  }

  "cosine similarity less than 50 " should "be less than fifty percent similar" in {
    val one = "date num user xrdp daemon info debug closed socket af_inet"
    val two = "date num user xrdp daemon info time info using default x key connection received from port"
    assertTrue(Features.findCosineSimilarity(one, two) < 0.50d)
  }

  "cosine similarity between 50 and 75 percent" should "greater than fifty percent similar" in {
    val one = "date num user xrdp daemon info time closed socket af_inet"
    val two = "date num user xrdp daemon info time info using default x key connection received from port"
    assertTrue(Features.findCosineSimilarity(one, two) > 0.50d)
  }

  "cosine similarity greater than 75 percent" should "return > 0.75d" in {
    assertTrue(Features.findCosineSimilarity("this is a test", "this is also a test") > 0.75d)
  }

  "test" should "" in {
    val gd = new GrokDetector().getCompiler()

    println("HERE")
//    val input = """2016-09-26 16:30:50,153 INFO org.apache.hadoop.hdfs.server.datanode.DataNode: Successfully sent block report 0x4a99afb4ecf3a,  containing 1 storage report(s), of which we sent 1. The reports had 102 total blocks and used 1 RPC(s). This took 1 msec to generate and 1 msecs for RPC and NN processing. Got back one command: FinalizeCommand/5."""
//    val g = gd.compile("%{TIMESTAMP_ISO8601:timeOne} %{LOGLEVEL:loglevelOne} %{JAVACLASS:javaOne}: Successfully sent block report ")
//    val c = g.capture(input)
//    println(c)


    val input = "2016-09-26 16:30:50,153 INFO org.apache.hadoop.hdfs.server.datanode.DataNode: Successfully sent block report 0x4a99afb4ecf3a,  containing 1 storage report(s), of which we sent 1. The reports had 102 total blocks and used 1 RPC(s). This took 1 msec to generate and 1 msecs for RPC and NN processing. Got back one command: FinalizeCommand/5."
//    val g = gd.compile(".*%{LOGLEVEL:loglevelOne} %{JAVACLASS:javaOne}: Successfully sent block report ")
//    val c = g.capture(input)
//    println(c)

    val grokCompiler = GrokCompiler.newInstance()
    grokCompiler.registerDefaultPatterns()

    val p = grokCompiler.getPatternDefinitions
    for(k <- p.keySet().asScala){
      println(s"k = ${k} and v = ${p.get(k)}")

    }

//    grokCompiler.register( Source.fromFile( s"/home/matt/temp/workflow/analytics/scala/log-likelihood/src/main/resources/dates.grok" ).reader() )
    grokCompiler.register( Source.fromFile( s"/home/matt/temp/workflow/analytics/scala/log-likelihood/src/main/resources/java.grok" ).reader() )
//    grokCompiler.register( Source.fromFile( s"/home/matt/temp/workflow/analytics/scala/log-likelihood/src/main/resources/numbers.grok" ).reader() )


    val g = gd.compile("%{TIMESTAMP_ISO8601:timeOne} %{LOGLEVEL:loglevelOne} %{JAVACLASS:javaOne}: Successfully sent block report .*%{POSINT:numberOne} storage report\\(s\\), of which we sent .*The reports had %{POSINT:numberTwo} total blocks and used %{POSINT:numberThree} RPC\\(s\\)\\. This took %{POSINT:numberFour} msec to generate and %{POSINT:numberFive} msecs for RPC and NN processing\\. Got back one command: .*")
    val c = g.capture(input)
    println(c)


  }
}