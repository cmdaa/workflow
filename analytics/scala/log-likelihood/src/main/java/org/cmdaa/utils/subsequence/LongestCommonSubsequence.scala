package org.cmdaa.utils.subsequence

object LongestCommonSubsequence {

  case class Memoized[A1, A2, B](f: (A1, A2) => B) extends ((A1, A2) => B) {
    val cache = scala.collection.mutable.Map.empty[(A1, A2), B]

    def apply(x: A1, y: A2) = cache.getOrElseUpdate((x, y), f(x, y))
  }

  lazy val lcsM: Memoized[List[String], List[String], List[String]] = Memoized {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (x :: xs, y :: ys) if x == y => x :: lcsM(xs, ys)
    case (x :: xs, y :: ys) => {
      (lcsM(x :: xs, ys), lcsM(xs, y :: ys)) match {
        case (xs, ys) if xs.length > ys.length => xs
        case (xs, ys) => ys
      }
    }
  }
  val A = "ACBDEA"
  val B = "ABCDA"

//  System.out.println("LCS :" + lcsM("test this is a test that should work".split(" ").toList, "test is a another test that may or may not work".split(" ").toList).mkString(" "))

  def find(string1: String, string2: String): String = {
    lcsM(string1.split(" ").toList, string2.split(" ").toList).mkString(" ")
  }

}