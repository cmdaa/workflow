# Installing the CMDAA application via Helm

## Add DNS entry to your machine using DNSMasq(optional)
#### Install dnsmasq
  ```
  brew update # Always update Homebrew and the formulae first
  brew install dnsmasq
  ```
  
#### Start dnsmasq
  ```
  sudo brew services start dnsmasq 
  ```
  
#### Configure dnsmasq
Edit /usr/local/etc/dnsmasq.conf and add the following line.
  ```
  conf-dir=/usr/local/etc/dnsmasq.d,*.conf
  ```
  
#### Add the following file to your configuration
  ```
  touch /usr/local/etc/dnsmasq.d/test.conf
  ```
  
#### Add the following line to the test.conf
   ```
   address=/.test/127.0.0.1 
   ```

#### Restart Brew
   ```
   sudo brew services restart dnsmasq
   ```
   
#### Set up your resolver
   ```
   sudo mkdir /etc/resolver
   ```

#### Add the following file to your resolver configuration
  ```
  touch /etc/resolver/test
  ```
   
#### Add the following line to your test resolver configuration
  ```
  address=/.test/127.0.0.1 
  ```

## Helm Install CMDAA
Add the google Helm repo and the argo helm repo to your Helm repo list and then use helm to install all the applications.
We call the google repo "stable" and the argo repo "argo."  Also NOTE: we need a specific version of minio (2.4.2)
The last line uses "test" as the DN value.  This assumes you use test as your base dn as described above. You may use a different value.
  ```
  helm repo add argo https://argoproj.github.io/argo-helm
  helm repo add stable https://kubernetes-charts.storage.googleapis.com
  helm install --name argo --namespace cmdaa argo/argo
  helm install stable/minio --name cmdaa-artifacts --namespace cmdaa  --set service.type=LoadBalancer --set persistence.enabled=false --version 2.4.2
  helm install --name cmdaa --namespace cmdaa helm-charts/cmdaa --set service.dn=test
  ```
  
Navigate Here  
  
## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 

