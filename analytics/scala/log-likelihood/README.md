# LogLikely - Analytics 

## Analytics
  * This is collection of analytics that can be run against a users log files.

## List of Analytics
  * [Log Likelihood](https://gitlab.com/cmdaa/workflow/blob/master/analytics/scala/log-likelihood/src/main/java/org/cmdaa/scala/analytics/LogLikelihood.scala) - Check to see what patterns are present in a certain class of log messages.
  * [Cosine Similarity](https://gitlab.com/cmdaa/workflow/blob/master/analytics/scala/log-likelihood/src/main/java/org/cmdaa/scala/analytics/LogLikelihoodConnectComponents.scala) - Check to see how similar identified classes of log messages are.
  * [Connected Components](https://gitlab.com/cmdaa/workflow/blob/master/analytics/scala/log-likelihood/src/main/java/org/cmdaa/scala/analytics/LogLikelihoodConnectComponents.scala) - Cluster like classes of log messages and find a commonality.
  * [Multi Line Detection](https://gitlab.com/cmdaa/workflow/blob/master/analytics/scala/log-likelihood/src/main/java/org/cmdaa/scala/analytics/DetectMultilineMessages2.scala) - What patterns of log messages occur in your system consistently in a particular order?

## Related Projects
   * [Data Pull](https://gitlab.com/cmdaa/workflow/tree/master/data-pull) - The first step in the analytic process.  This project dedicated to pulling logs from your log data source.

## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 
