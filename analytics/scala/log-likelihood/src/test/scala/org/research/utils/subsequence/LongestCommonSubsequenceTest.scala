package org.research.utils.subsequence

import org.cmdaa.utils.subsequence.LongestCommonSubsequence
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LongestCommonSubsequenceTest extends FlatSpec {

  val groks = "2015-10-18 18:03:57,564 INFO [IPC Server handler 12 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000006 asked for a task" ::
              "2015-10-18 18:04:11,643 INFO [IPC Server handler 23 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000010 asked for a task" ::
              "2015-10-18 18:02:12,855 INFO [IPC Server handler 13 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000004 given task: attempt_1445144423722_0020_m_000002_0" ::
              "2015-10-18 18:02:05,870 INFO [IPC Server handler 27 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000003 given task: attempt_1445144423722_0020_m_000001_0" ::
              "2015-10-18 18:02:02,510 INFO [IPC Server handler 13 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000002 given task: attempt_1445144423722_0020_m_000000_0" ::
              "2015-10-18 18:03:57,564 INFO [IPC Server handler 20 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000007 asked for a task" ::
              "2015-10-18 18:02:02,510 INFO [IPC Server handler 13 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000002 asked for a task" ::
              "2015-10-18 18:04:12,143 INFO [IPC Server handler 10 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000011 asked for a task" ::
              "2015-10-18 18:02:50,560 INFO [IPC Server handler 9 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000005 given task: attempt_1445144423722_0020_m_000003_0" ::
              "2015-10-18 18:04:11,643 INFO [IPC Server handler 23 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000010 given task: attempt_1445144423722_0020_m_000008_0" ::
              Nil

//    val groks = "%{TIMESTAMP_ISO8601:timeOne} %{LOGLEVEL:loglevelOne} \\[IPC Server handler %{POSINT:numberOne} on %{POSINT:numberTwo}\\] %{JAVACLASS:javaOne}: JVM with ID : .*asked for a task" ::
//                "%{TIMESTAMP_ISO8601:timeOne} %{LOGLEVEL:loglevelOne} \\[IPC Server handler %{POSINT:numberOne} on %{POSINT:numberTwo}\\] %{JAVACLASS:javaOne}: JVM with ID : .*asked for a task" ::
//                "%{TIMESTAMP_ISO8601:timeOne} %{LOGLEVEL:loglevelOne} \\[IPC Server handler %{POSINT:numberOne} on %{POSINT:numberTwo}\\] %{JAVACLASS:javaOne}: JVM with ID : .*asked for a task" ::
//                "2015-10-18 18:02:05,870 INFO [IPC Server handler 27 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000003 given task: attempt_1445144423722_0020_m_000001_0" ::
//                "2015-10-18 18:02:02,510 INFO [IPC Server handler 13 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000002 given task: attempt_1445144423722_0020_m_000000_0" ::
//                "2015-10-18 18:03:57,564 INFO [IPC Server handler 20 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000007 asked for a task" ::
//                "2015-10-18 18:02:02,510 INFO [IPC Server handler 13 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000002 asked for a task" ::
//                "2015-10-18 18:04:12,143 INFO [IPC Server handler 10 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID : jvm_1445144423722_0020_m_000011 asked for a task" ::
//                "2015-10-18 18:02:50,560 INFO [IPC Server handler 9 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000005 given task: attempt_1445144423722_0020_m_000003_0" ::
//                "2015-10-18 18:04:11,643 INFO [IPC Server handler 23 on 62270] org.apache.hadoop.mapred.TaskAttemptListenerImpl: JVM with ID: jvm_1445144423722_0020_m_000010 given task: attempt_1445144423722_0020_m_000008_0" ::
//                Nil


  "LCS" should "work" in {
    val out = foo()
      println(out.toString)
  }

  def foo(): String = {
    var l = LongestCommonSubsequence
    var tempGrok = ""

    if( !groks.isEmpty ){
      tempGrok = groks(0)
      groks.foreach( in => {
        tempGrok = l.find( tempGrok, in)
      })
    }
    return tempGrok
  }
}