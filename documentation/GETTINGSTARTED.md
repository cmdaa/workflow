# Getting Started with LogLikely

# With Docker
If you already have [docker](https://www.docker.com/) installed, then just [follow these directions](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLINGDOCKER.md).

# Within Kubernetes
It is recommended that you install CMDAA application into kubernetes using this [Helm Chart](https://gitlab.com/cmdaa/helm-charts).

# On Bare Metal
If you just want to run a simple jar on some data you have lieing around, then [give this a try](https://gitlab.com/cmdaa/workflow/blob/master/documentation/BUILDINGPROJECT.md).
