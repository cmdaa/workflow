package org.cmdaa.utils

import java.util.UUID

import org.apache.commons.math3.distribution.ChiSquaredDistribution
import org.cmdaa.pojos.{LogLikelihoodLine, LogLikelihoodOutput, Term}
import org.cmdaa.scala.analytics.LogLikelihood
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.utils.grok.GrokDetector

import scala.collection.JavaConversions.mapAsScalaMap
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.collection.parallel.ParMap

/**
  * Object containing various methods that process LogLikelihoodOutput state
  *  
  * Created on 1/23/18.
  */
object LoglikelihoodUtils {
//  val currentDirectory = new java.io.File(".").getCanonicalPath

//  val grokDetector = new GrokDetector(s"${currentDirectory}/src/main/resources/")
  val grokDetector = new GrokDetector( scala.util.Properties.envOrElse( "GROK_CONF_DIR","/cmdaa/conf/groks" ) )

  /**
    * Calculates the cumulative probability of the word occurring in the document collection
    *
    * @param dococc   number of times word occurs in the document
    * @param doctotal total number of words in the document
    * @param allocc   number of times word occurs in document collection
    * @param alltotal total number of words in the document collection
    * @return the cumulative probability of the word occurring in the document collection
    */
  def computeLogLikelihood(dococc: Int, doctotal: Int, allocc: Int, alltotal: Int): Double = {

    val docOccDouble = dococc.toDouble
    val docTotDouble = doctotal.toDouble
    val allOccDouble = allocc.toDouble
    val allTotDouble = alltotal.toDouble

    val chi: ChiSquaredDistribution = new ChiSquaredDistribution(1d)
    val p = docOccDouble / docTotDouble
    val q = allOccDouble / allTotDouble

    if (p < q) return 0

    val t = (docOccDouble + allOccDouble) / (docTotDouble + allTotDouble)

    if (t == 0) {
      println("attempted division by zero.")
      return 0d
    }

    var v = docOccDouble * Math.log(p / t) + allOccDouble * Math.log(q / t)

    if (t == 1) return 2d * v

    if (p < 1) v += (docTotDouble - docOccDouble) * Math.log((1 - p) / (1 - t))
    if (q < 1) v += (allTotDouble - allOccDouble) * Math.log((1 - q) / (1 - t))
    v += v

    return 1d - chi.cumulativeProbability(v)
  }

  /**
    * The reformatString method returns a new String with the following updates:
    * 1. Replace hdfs:// or file:// directory string with DIR
    * 2. If removeAlpha = 0, simply replaces all numerics with NUM
    * 3. If removeNonAlpha = 1, removes non-alphanumeric characters
    *
    * @param inputString    the String to be formatted
    * @param removeNonAlpha LoglikelihookConf.removeNonAlpha value
    * @return String formatted for LogLikelihoodLine.shortenedMessage
    */
  def reformatString(inputString: String, removeNonAlpha: Boolean): String = {
    var s = new ListBuffer[String]
//    var dateExtractor = new GrokExtractor()
    var valToSplit = inputString

    // remove all directories from the string
    for (v <- valToSplit.split(" ")) {
      if (v.contains("hdfs://") || v.contains("file://")) s += s"DIR" else s += s"${v}"
    }

    if (removeNonAlpha) {
      // replace data strings with the word DATE
//      valToSplit = dateExtractor.getReformattedDateTimeString(s.mkString(" "))
      valToSplit = grokDetector.detectAllGroks(s.mkString(" "), false)
      s.clear()
      return valToSplit.replaceAll("[^A-Za-z ]", "")
    } else {
      return grokDetector.detectAllGroks(s.mkString(" "), false).replaceAll("[0-9]+", "NUM")
    }
  }

  /**
    * Processes line from log message and returns a LogLikelihoodLine object
    *
    * @param string    original log file message to be processed
    * @param conf      the LogLikelihoodConf object used for biz logic
    * @param wordCount mappings of word counts used for biz logic
    * @param output    LogLikelihoodOutput object used for biz logic
    * @return LogLikelihood object
    */
  def processLine(string: String, conf: LogLikelihoodConf, wordCount: ParMap[String, Int], output: LogLikelihoodOutput): LogLikelihoodLine = {
    var line = new LogLikelihoodLine

    line.originalMessage = string
    line.shortenedMessage = reformatString(string, conf.removeNonAlpha.apply)
    line.keyString = generateKeyString(line, conf, wordCount, output)
    line.hash = generateHash(line.keyString)


    val check = line.originalMessage.toLowerCase()

    val terms:ArrayBuffer[Term] = new ArrayBuffer[Term]


    line.shortenedMessage.split("\\W+").foreach( v => {
      val t: Term = new Term()

      if (check.contains(v.trim.toLowerCase())) {
        t.preProcessed = false
        t.term = v.trim
        t.score = line.logLikelihoodVals.get(v.trim)
      } else {
        t.preProcessed = true
        t.term = v.trim
        t.score = line.logLikelihoodVals.get(v.trim)
      }

      if( line.keyString.contains(v.trim.toLowerCase() ) ){
        t.usedForClustering = true
      }

      terms += t

    })

    line.terms = terms.toArray

    line
  }

  /**
    * Generates the LogLikelihoodLine keyString attribute
    *
    * @param string    original log file message to be processed
    * @param conf      the LogLikelihoodConf object used for biz logic
    * @param wordCount mappings of word counts used for biz logic
    * @param output    LogLikelihoodOutput object used for biz logic
    */
  def generateKeyString(line: LogLikelihoodLine, conf: LogLikelihoodConf, wordCount: ParMap[String, Int], output: LogLikelihoodOutput): String = {
    line.shortenedMessage.split("\\W+").map(w => {
      val key = w.toLowerCase()
      line.logLikelihoodVals += (key -> LogLikelihood.DEC_FORMAT.format(LoglikelihoodUtils.
        computeLogLikelihood(1, output.totalLogLines, wordCount.get(key).get, output.totalWords)).toDouble)
      if (line.logLikelihoodVals.get(key) < conf.likelihoodMax.apply()) key else null
    }).filter(_ != null).toList.mkString(" ")
  }

  /**
    * Generates the LogLikelihoodLine hash attribute
    *
    * @param line LogLikelihood object to set the hash value
    * @return hash String
    */
  def generateHash(keyString: String): String = {
    UUID.nameUUIDFromBytes(keyString.getBytes()).toString
  }
}