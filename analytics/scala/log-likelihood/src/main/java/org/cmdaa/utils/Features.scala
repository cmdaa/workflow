package org.cmdaa.utils

import org.apache.commons.text.similarity.CosineDistance
import org.apache.commons.text.similarity.CosineSimilarity

/**
  * Created on 1/26/18.
  */
object Features {

  /**
    * Calculates the cosine similarity of two strings that are converted to all lower-case characters.
    * Important Note: this method identifies the unique words in each string and then calculates
    * the cosine similarity
    */
  def findCosineSimilarity(string1: String, string2: String): Double = {
    new CosineSimilarity().cosineSimilarity(WordUtils.generateUniqueWordsMap(string1), WordUtils.generateUniqueWordsMap(string2))
  }
}
