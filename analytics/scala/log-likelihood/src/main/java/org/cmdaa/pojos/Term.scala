package org.cmdaa.pojos

class Term {
  var term: String = ""
  var preProcessed: Boolean = false
  var score: Double = 0.0d
  var usedForClustering: Boolean = false
}
