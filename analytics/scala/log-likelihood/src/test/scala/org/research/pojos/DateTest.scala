package org.research.pojos

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}

//import com.joestelmach.natty.{DateGroup, Parser}

//import org.ocpsoft.prettytime.nlp.PrettyTimeParser

@RunWith(classOf[JUnitRunner])
class DateTest extends FunSuite with BeforeAndAfterAll {

 //     var dtr = new DateTimeRegex

  override def beforeAll = {
  }

  test("test messages") {
//    val parser = new Parser()
//
//    var l: java.util.List[DateGroup] = parser.parse("03-17 16:13:38.811 1702 2395 D WindowManager: printFreezingDisplayLogsopening app wtoken = AppWindowToken{9f4ef63 token=Token{a64f992 ActivityRecord{de9231d u0 com.tencent.qt.qtl/.activity.info.NewsDetailXmlActivity t761}}}, allDrawn= false, startingDisplayed = false, startingMoved = false, isRelaunching = false")
//
//    println("---")
//    for( a <- 0 to l.size() - 1){
//      println( s"${l.get(a).getDates() } + ${l.get(a).getText() }" )
//    }
//    println("---")
//
////    if (!l.isEmpty) {
////      println(l.get(0).getDates.get(0) + "----" + l.get(0).getText)
////    } else {
////      ""
////    }
//
//    l= parser.parse("[Sun Dec 04 04:47:44 2005] [notice] workerEnv.init() ok /etc/httpd/conf/workers2.properties")
//
//    println("---")
//    for( a <- 0 to l.size() - 1){
//      println( s"${l.get(a).getDates() } + ${l.get(a).getText() }" )
//    }
//    println("---")
//
//    l= parser.parse("- 1117838570 2005.06.03 R02-M1-N0-C:J12-U11 DATE R02-M1-N0-C:J12-U11 RAS KERNEL INFO instruction cache parity error corrected")
//
//    println("---")
//    for( a <- 0 to l.size() - 1){
//      println( s"${l.get(a).getDates() } + ${l.get(a).getText}" )
//    }
//    println("---")


        import org.ocpsoft.prettytime.nlp.PrettyTimeParser
//        var dates = new PrettyTimeParser().parse("03-17 16:13:38.811 1702 2395 D WindowManager: printFreezingDisplayLogsopening app wtoken = AppWindowToken{9f4ef63 token=Token{a64f992 ActivityRecord{de9231d u0 com.tencent.qt.qtl/.activity.info.NewsDetailXmlActivity t761}}}, allDrawn= false, startingDisplayed = false, startingMoved = false, isRelaunching = false")
//        println(dates)
//
//        dates = new PrettyTimeParser().parse("[Sun Dec 04 04:47:44 2005] [notice] workerEnv.init() ok /etc/httpd/conf/workers2.properties")
//        println(dates)

        var parse = new PrettyTimeParser().parseSyntax("03-17 16:13:38.811 1702 2395 D WindowManager: printFreezingDisplayLogsopening app wtoken = AppWindowToken{9f4ef63 token=Token{a64f992 ActivityRecord{de9231d u0 com.tencent.qt.qtl/.activity.info.NewsDetailXmlActivity t761}}}, allDrawn= false, startingDisplayed = false, startingMoved = false, isRelaunching = false")

        println("---")
        println("03-17 16:13:38.811 1702 2395 D WindowManager: printFreezingDisplayLogsopening app wtoken = AppWindowToken{9f4ef63 token=Token{a64f992 ActivityRecord{de9231d u0 com.tencent.qt.qtl/.activity.info.NewsDetailXmlActivity t761}}}, allDrawn= false, startingDisplayed = false, startingMoved = false, isRelaunching = false")
        for( a <- 0 to parse.size() - 1){
          println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")

        parse = new PrettyTimeParser().parseSyntax("[Sun Dec 04 04:47:44 2005] [notice] workerEnv.init() ok /etc/httpd/conf/workers2.properties")
        println("---")
        println("[Sun Dec 04 04:47:44 2005] [notice] workerEnv.init() ok /etc/httpd/conf/workers2.properties")
        for( a <- 0 to parse.size() - 1){
          println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")

        parse = new PrettyTimeParser().parseSyntax("- 1117838570 2005.06.03 R02-M1-N0-C:J12-U11 2005-06-03-15.42.50.675872 R02-M1-N0-C:J12-U11 RAS KERNEL INFO instruction cache parity error corrected")

        println("---")
        println("- 1117838570 2005.06.03 R02-M1-N0-C:J12-U11 2005-06-03-15.42.50.675872 R02-M1-N0-C:J12-U11 RAS KERNEL INFO instruction cache parity error corrected")
        for( a <- 0 to parse.size() - 1){
          println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")

        parse = new PrettyTimeParser().parseSyntax("081109 203615 148 INFO dfs.DataNode$PacketResponder: PacketResponder 1 for block blk_38865049064139660 terminating")

        println("---")
        println("081109 203615 148 INFO dfs.DataNode$PacketResponder: PacketResponder 1 for block blk_38865049064139660 terminating")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("134681 node-246 unix.hw state_change.unavailable 1077804742 1 Component State Change: Component \\042SCSI-WWID:01000010:6005-08b4-0001-00c6-0006-3000-003d-0000\\042 is in the unavailable state (HWID=1973)")

        println("---")
        println("134681 node-246 unix.hw state_change.unavailable 1077804742 1 Component State Change: Component \\042SCSI-WWID:01000010:6005-08b4-0001-00c6-0006-3000-003d-0000\\042 is in the unavailable state (HWID=1973)")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("2015-10-18 18:01:47,978 INFO [main] org.apache.hadoop.mapreduce.v2.app.MRAppMaster: Created MRAppMaster for application appattempt_1445144423722_0020_000001")

        println("---")
        println("2015-10-18 18:01:47,978 INFO [main] org.apache.hadoop.mapreduce.v2.app.MRAppMaster: Created MRAppMaster for application appattempt_1445144423722_0020_000001")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")

        parse = new PrettyTimeParser().parseSyntax("20171223-22:15:29:606|Step_LSC|30002312|onStandStepChanged 3579")

        println("---")
        println("20171223-22:15:29:606|Step_LSC|30002312|onStandStepChanged 3579")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("Jun 14 15:16:01 combo sshd(pam_unix)[19939]: authentication failure; logname= uid=0 euid=0 tty=NODEVssh ruser= rhost=218.188.2.4 ")

        println("---")
        println("Jun 14 15:16:01 combo sshd(pam_unix)[19939]: authentication failure; logname= uid=0 euid=0 tty=NODEVssh ruser= rhost=218.188.2.4 ")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("Jul  1 09:00:55 calvisitor-10-105-160-95 kernel[0]: IOThunderboltSwitch<0>(0x0)::listenerCallback - Thunderbolt HPD packet for route = 0x0 port = 11 unplug = 0\nJul 1 09:01:05 calvisitor-10-105-160-95 com.apple.CDScheduler[43]: Thermal pressure state: 1 Memory pressure state: 0")

        println("---")
        println("Jul  1 09:00:55 calvisitor-10-105-160-95 kernel[0]: IOThunderboltSwitch<0>(0x0)::listenerCallback - Thunderbolt HPD packet for route = 0x0 port = 11 unplug = 0\nJul 1 09:01:05 calvisitor-10-105-160-95 com.apple.CDScheduler[43]: Thermal pressure state: 1 Memory pressure state: 0")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("Dec 10 06:55:46 LabSZ sshd[24200]: reverse mapping checking getaddrinfo for ns.marryaldkfaczcz.com [173.234.31.186] failed - POSSIBLE BREAK-IN ATTEMPT!")

        println("---")
        println("Dec 10 06:55:46 LabSZ sshd[24200]: reverse mapping checking getaddrinfo for ns.marryaldkfaczcz.com [173.234.31.186] failed - POSSIBLE BREAK-IN ATTEMPT!")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("nova-api.log.1.2017-05-16_13:53:08 2017-05-16 00:00:00.008 25746 INFO nova.osapi_compute.wsgi.server [req-38101a0b-2096-447d-96ea-a692162415ae 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] 10.11.10.1 \"GET /v2/54fadb412c4e40cdbaed9335e4c35a9e/servers/detail HTTP/1.1\" status: 200 len: 1893 time: 0.2477829")

        println("---")
        println("nova-api.log.1.2017-05-16_13:53:08 2017-05-16 00:00:00.008 25746 INFO nova.osapi_compute.wsgi.server [req-38101a0b-2096-447d-96ea-a692162415ae 113d3a99c3da401fbd62cc2caa5b96d2 54fadb412c4e40cdbaed9335e4c35a9e - - -] 10.11.10.1 \"GET /v2/54fadb412c4e40cdbaed9335e4c35a9e/servers/detail HTTP/1.1\" status: 200 len: 1893 time: 0.2477829")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("[10.30 16:49:06] chrome.exe - proxy.cse.cuhk.edu.hk:5070 open through proxy proxy.cse.cuhk.edu.hk:5070 HTTPS")

        println("---")
        println("[10.30 16:49:06] chrome.exe - proxy.cse.cuhk.edu.hk:5070 open through proxy proxy.cse.cuhk.edu.hk:5070 HTTPS")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("17/06/09 20:10:40 INFO executor.CoarseGrainedExecutorBackend: Registered signal handlers for [TERM, HUP, INT]")

        println("---")
        println("17/06/09 20:10:40 INFO executor.CoarseGrainedExecutorBackend: Registered signal handlers for [TERM, HUP, INT]")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("17/06/09 20:10:40 INFO executor.CoarseGrainedExecutorBackend: Registered signal handlers for [TERM, HUP, INT]")

        println("---")
        println("17/06/09 20:10:40 INFO executor.CoarseGrainedExecutorBackend: Registered signal handlers for [TERM, HUP, INT]")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("- 1131566461 2005.11.09 dn228 Nov 9 12:01:01 dn228/dn228 crond(pam_unix)[2915]: session closed for user root")

        println("---")
        println("- 1131566461 2005.11.09 dn228 Nov 9 12:01:01 dn228/dn228 crond(pam_unix)[2915]: session closed for user root")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("2016-09-28 04:30:30, Info CBS Loaded Servicing Stack v6.1.7601.23505 with Core: C:\\Windows\\winsxs\\amd64_microsoft-windows-servicingstack_31bf3856ad364e35_6.1.7601.23505_none_681aa442f6fed7f0\\cbscore.dll")

        println("---")
        println("2016-09-28 04:30:30, Info CBS Loaded Servicing Stack v6.1.7601.23505 with Core: C:\\Windows\\winsxs\\amd64_microsoft-windows-servicingstack_31bf3856ad364e35_6.1.7601.23505_none_681aa442f6fed7f0\\cbscore.dll")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")



        parse = new PrettyTimeParser().parseSyntax("2015-07-29 17:41:44,747 - INFO [QuorumPeer[myid=1]/0:0:0:0:0:0:0:0:2181:FastLeaderElection@774] - Notification time out: 3200")

        println("---")
        println("2015-07-29 17:41:44,747 - INFO [QuorumPeer[myid=1]/0:0:0:0:0:0:0:0:2181:FastLeaderElection@774] - Notification time out: 3200")
        for( a <- 0 to parse.size() - 1){
              println( s"${parse.get(a).getDates() } + ${parse.get(a).getText() }" )
        }
        println("---")


//        assertFalse(parse.isEmpty)
//        println( parse.get(0).getDates )


//        val formatted = new PrettyTimeParser().format(parse.get(0).getDates.get(0))
//        assertEquals(14, parse.get(0).getPosition)
//        assertEquals(1, parse.get(0).getDates.size)
//        assertNull(parse.get(0).getRecursUntil)
//        assertTrue(parse.get(0).isRecurring)
//        assertEquals(1000 * 60 * 60 * 24 * 3, parse.get(0).getRecurInterval)
  }
}
