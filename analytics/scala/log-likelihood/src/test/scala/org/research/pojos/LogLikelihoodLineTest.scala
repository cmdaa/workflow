package org.cmdaa.pojos

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterAll
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.junit.Assert._


@RunWith(classOf[JUnitRunner])
class LogLikelihoodLineTest extends FunSuite with BeforeAndAfterAll {
  var line: LogLikelihoodLine = _

  override def beforeAll = {
    line = new LogLikelihoodLine()
    line.originalMessage = "This is the 2018 message"
    line.shortenedMessage = "This is the message"
    line.keyString = "this is the message"
  }

  test("test messages") {
    assertEquals("This is the 2018 message", line.originalMessage)
    assertEquals("This is the message", line.shortenedMessage)
  }

  test("test canEqual") {
    assertTrue(line.canEqual(new LogLikelihoodLine()))
  }

  test("test equals") {
    val dupe = new LogLikelihoodLine()
    dupe.originalMessage = "This is the 2018 message"
    dupe.shortenedMessage = "This is the message"
    dupe.keyString = "this is the message"
    assertTrue(line.equals(dupe))
  }

  test("test hashCode") {
    val dupe = new LogLikelihoodLine()
    dupe.originalMessage = "This is the 2018 message"
    dupe.shortenedMessage = "This is the message"
    dupe.keyString = "this is the message"

    assertEquals(-626536668, line.hashCode())
    assertEquals(-626536668, dupe.hashCode())
    assertEquals(line.hashCode(), dupe.hashCode())
  }
}