package org.cmdaa.utils.multiline

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import org.junit.Assert._
import scala.collection.mutable.ListBuffer

/**
  * Created on 3/27/18.
  */
@RunWith( classOf[JUnitRunner] )
class MultiLineDetectorTest extends FlatSpec {

  "prepend with 3rd element" should "prepend" in {
    /*
     * Since log messages 1a and 1b are added in the first multi-line 
     * pair and 1a is the first in the pair, log message 1c should prepend 1a
     */
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1c", "log message 1a")

    assertEquals(1, f.getLines.size)
    assertEquals(3, f.getLines()(0).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))

    val expected = List("1c", "1a", "1b").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "append with 3rd element" should "append" in {
    /*
     * Since log messages 1a and 1b are added in the first multi-line 
     * pair and 1b is the second in the pair, log message 1c 
     * should be appended after 1b
     */
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    assertEquals(1, f.getLines.size)
    assertEquals(3, f.getLines()(0).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))

    val expected = List("1a", "1b", "1c").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "append with 3rd, 4th, 5th element" should "append" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")
    f.insert("log message 1c", "log message 1d")
    f.insert("log message 1d", "log message 1e")

    assertEquals(1, f.getLines().size)
    assertEquals(5, f.getLines()(0).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))
    assertEquals(0, f.getListPos("log message 1d"))
    assertEquals(0, f.getListPos("log message 1e"))

    val expected = List("1a", "1b", "1c", "1d", "1e").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }


  "invalid reversed order of 2nd and 3rd elements" should "not update" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1c", "log message 1b")

    assertEquals(1, f.getLines.size)
    assertEquals(2, f.getLines()(0).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(-1, f.getListPos("log message 1c"))

    val expected = List("1a", "1b").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "invalid reversed order with duplicate 1st and 2nd elements" should "not update" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1a")

    assertEquals(1, f.getLines.size)
    assertEquals(2, f.getLines()(0).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))

    val expected = List("1a", "1b").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "invalid does not append trailing element" should "not update" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    // this should be invalid because it does not append trailing element
    f.insert("log message 1b", "log message 1d")

    assertEquals(3, f.getLines()(0).size)
    assertEquals(1, f.getLines().size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))
    assertEquals(-1, f.getListPos("log message 1d"))

    val expected = List("1a", "1b", "1c").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "invalid does not prepend leading element" should "not update" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    // this should be invalid because it does not prepend leading element
    f.insert("log message 1d", "log message 1b")

    assertEquals(3, f.getLines()(0).size)
    assertEquals(1, f.getLines().size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))
    assertEquals(-1, f.getListPos("log message 1d"))

    val expected = List("1a", "1b", "1c").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "two line groups with append" should " result in two line lists" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    f.insert("log message 2a", "log message 2b")
    f.insert("log message 2b", "log message 2c")

    assertEquals(2, f.getLines().size)
    assertEquals(3, f.getLines()(0).size)
    assertEquals(3, f.getLines()(1).size)

    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))
    assertEquals(1, f.getListPos("log message 2a"))
    assertEquals(1, f.getListPos("log message 2b"))
    assertEquals(1, f.getListPos("log message 2c"))

    val expected1 = List("1a", "1b", "1c").iterator
    val expected2 = List("2a", "2b", "2c").iterator

    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected1.next()}", elem)
    }

    for (elem <- f.getLines()(1)) {
      assertEquals(s"log message ${expected2.next()}", elem)
    }
  }

  "invalid multiline with dupes" should "not update" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")
    f.insert("log message 1c", "log message 1d")
    f.insert("log message 1d", "log message 1e")

    assertEquals(1, f.getLines().size)
    assertEquals(5, f.getLines()(0).size)
    assertEquals(0, f.getListPos("log message 1a"))
    assertEquals(0, f.getListPos("log message 1b"))
    assertEquals(0, f.getListPos("log message 1c"))
    assertEquals(0, f.getListPos("log message 1d"))
    assertEquals(0, f.getListPos("log message 1e"))

    val expected = List("1a", "1b", "1c", "1d", "1e").iterator

    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")
    f.insert("log message 1d", "log message 1c")

    assertEquals(1, f.getLines().size)
    assertEquals(5, f.getLines()(0).size)
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected.next()}", elem)
    }
  }

  "invalid attempt to append element to two line groups" should "only append to first group" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    f.insert("log message 2a", "log message 2b")
    f.insert("log message 2b", "log message 1c")

    assertEquals(2, f.getLines.size)
    assertEquals(3, f.getLines()(0).size)
    assertEquals(2, f.getLines()(1).size)
    assertEquals(0, f.getListPos("log message 1c"))

    val expected1 = List("1a", "1b", "1c").iterator
    val expected2 = List("2a", "2b").iterator
    for (elem <- f.getLines()(0)) {
      assertEquals(s"log message ${expected1.next()}", elem)
    }

    for (elem <- f.getLines()(1)) {
      assertEquals(s"log message ${expected2.next()}", elem)
    }
  }

  "key list one line group" should "return a key list size 1 with 3 elements" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")

    assertEquals(1, f.getMessageKeyList().size)
    assertEquals(List("log message 1a log message 1b log message 1c"), f.getMessageKeyList())
  }

  "key list two line groups" should "return a key list size 2 with 3 elements each" in {
    val f = new MultiLineDetector
    f.insert("log message 1a", "log message 1b")
    f.insert("log message 1b", "log message 1c")
    f.insert("log message 2a", "log message 2b")
    f.insert("log message 2b", "log message 2c")

    assertEquals(2, f.getMessageKeyList().size)
    assertEquals(List("log message 1a log message 1b log message 1c", "log message 2a log message 2b log message 2c"),
      f.getMessageKeyList())
  }

  "sliding window" should "return size of 7" in {
    val f = new MultiLineDetector
    // a -> g : 7
    f.insert("log_message_1a", "log_message_1b")
    f.insert("log_message_1b", "log_message_1c")
    f.insert("log_message_1c", "log_message_1d")
    f.insert("log_message_1d", "log_message_1e")
    f.insert("log_message_1e", "log_message_1f")
    f.insert("log_message_1f", "log_message_1g")

    // a -> d : 4
    f.insert("log_message_2a", "log_message_2b")
    f.insert("log_message_2b", "log_message_2c")
    f.insert("log_message_2c", "log_message_2d")

    // a -> c : 3
    f.insert("log_message_3a", "log_message_3b")
    f.insert("log_message_3b", "log_message_3c")

    // a -> e : 5
    f.insert("log_message_4a", "log_message_4b")
    f.insert("log_message_4b", "log_message_4c")
    f.insert("log_message_4c", "log_message_4d")
    f.insert("log_message_4d", "log_message_4e")

    assertEquals(7, f.getSlidingWindow())
  }
}