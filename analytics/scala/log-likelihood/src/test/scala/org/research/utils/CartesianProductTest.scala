package org.cmdaa.utils

import com.google.gson.GsonBuilder
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.cmdaa.pojos.LogLikelihoodLine
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scalax.collection.GraphEdge._
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph
import org.scalatest.FunSuite
import org.junit.Assert._

/**
  * Created on 1/25/18.
  */
@RunWith(classOf[JUnitRunner])
class CartesianProductTest extends FunSuite {

  val cartesianProduct: CartesianProduct[LogLikelihoodLine] = new CartesianProduct[LogLikelihoodLine]()

  protected def createObj(value: String, shortValue: String): LogLikelihoodLine = {
    var line = new LogLikelihoodLine
    line.keyString = value
    line.originalMessage = value
    line.shortenedMessage = shortValue
    line.hash = shortValue
    line
  }

  protected def getList(start: Int, end: Int, others: List[LogLikelihoodLine]): List[LogLikelihoodLine] = {
    var buffer = new ListBuffer[LogLikelihoodLine]()
    for (i <- start to end) buffer += createObj(s"line ${i}", s"${i}")

    if (others != null)
      buffer ++= others

    buffer.toList
  }

  protected def assertCrossJoins(products: List[(LogLikelihoodLine, LogLikelihoodLine)]) = {
    products.zipWithIndex.foreach { case (line, i) => {
      i match {
        case 0 => {
          assertEquals("line 4", line._2.keyString)
        }
        case 1 => {
          assertEquals("line 5", line._2.keyString)
        }
        case 2 => {
          assertEquals("line 6", line._2.keyString)
        }
      }
    }
    }
  }

  protected def assertSelfJoins(products: List[(LogLikelihoodLine, LogLikelihoodLine)]) = {
    products.zipWithIndex.foreach { case (line, i) => {
      i match {
        case 0 => {
          assertEquals("line 1", line._2.keyString)
        }
        case 1 => {
          assertEquals("line 2", line._2.keyString)
        }
        case 2 => {
          assertEquals("line 3", line._2.keyString)
        }
      }
    }
    }
  }

  test("cartesian product of the same list") {
    val product = cartesianProduct.cross(getList(1, 3, null))

    val ones = product.filter(x => x._1.keyString.equals("line 1"))
    val twos = product.filter(x => x._1.keyString.equals("line 2"))
    val threes = product.filter(x => x._1.keyString.equals("line 3"))

    assertEquals(9, product.size)

    assertSelfJoins(ones)
    assertSelfJoins(twos)
    assertSelfJoins(threes)
  }

  test("cartesian product of the same list as a stream") {
    val product = cartesianProduct.cross(getList(1, 3, null)).toStream

    val ones = product.filter(x => x._1.keyString.equals("line 1"))
    val twos = product.filter(x => x._1.keyString.equals("line 2"))
    val threes = product.filter(x => x._1.keyString.equals("line 3"))

    assertEquals(9, product.toList.size)

    assertSelfJoins(ones.toList)
    assertSelfJoins(twos.toList)
    assertSelfJoins(threes.toList)
  }

  test("cartesian product of two different lists") {
    val product = cartesianProduct.cross(getList(1, 3, null), getList(4, 6, null))

    val ones = product.filter(x => x._1.keyString.equals("line 1"))
    val twos = product.filter(x => x._1.keyString.equals("line 2"))
    val threes = product.filter(x => x._1.keyString.equals("line 3"))

    assertEquals(9, product.size)

    assertCrossJoins(ones)
    assertCrossJoins(twos)
    assertCrossJoins(threes)
  }

  test("cosine similarities of self-join cartesian product") {
    val product = cartesianProduct.cross(getList(1, 3, null))

    assertEquals(0, product.map(in => (in._1.keyString + " " + in._2.keyString,
      Features.findCosineSimilarity(in._1.keyString, in._2.keyString))).filter(in => in._2 < 0.495d).size)

    assertEquals(6, product.map(in => (in._1.keyString + " " + in._2.keyString,
      Features.findCosineSimilarity(in._1.keyString, in._2.keyString))).filter(in => in._2 < 0.500d).size)
  }

  test("building graph from cartesian product") {
    val product = cartesianProduct.cross(getList(1, 3, null)).map(in => (
      Features.findCosineSimilarity(in._1.keyString, in._2.keyString), in._1, in._2))

    val cpGraph = Graph[Int, UnDiEdge](9)
    product.foreach(x => {
      cpGraph += (x._2.hashCode ~ x._3.hashCode)
    })

    assertFalse(cpGraph.isEmpty)
    assertFalse(cpGraph.isDirected)
    assertEquals(6, cpGraph.graphSize)
  }

  test("threshold generation of cartisian product") {
    val product = cartesianProduct.cross(getList(1, 3, null)).map(in => (
      Features.findCosineSimilarity(in._1.keyString, in._2.keyString), in._1, in._2))
    val similarities = List(0.49999999999999990, 0.99999999999999980)

    product.foreach(x => assertTrue(similarities.contains(x._1)))
  }

  test(" should ") {
    val THRESHOLD = 0.74d
    val a = getList(1, 4, List(createObj("line 1", "1"), createObj("line 2", "2")))

    var c1 = cartesianProduct.cross(a)
    var cartesian = c1.par.map(in => {
      if (in._1.keyString.hashCode < in._2.keyString.hashCode) (in._1, in._2) else (in._2, in._1)
    }).distinct

    var sims = cartesian.map(in => {
      val similarity = Features.findCosineSimilarity(in._1.keyString, in._2.keyString)
      if (similarity > THRESHOLD) {
        println(s"${in._1.keyString}/${in._2.keyString} = ${similarity}")
      }
      (similarity, in._1, in._2)
    }).filter(in => in._1 > THRESHOLD)

    val gson = new GsonBuilder().setPrettyPrinting().create()

    var g = Graph[Int, UnDiEdge](5)
    var mappedValues = new mutable.HashMap[Int, LogLikelihoodLine]()

    var count = 0
    sims.seq.map(in => {
      g += (in._2.keyString.hashCode ~ in._3.keyString.hashCode)
      mappedValues.put(in._2.keyString.hashCode, in._2)
      mappedValues.put(in._3.keyString.hashCode, in._3)
      count += 1
      if (count % 2 == 0) println(s"Added ${count} edges")
    })

    var objCount = 0
    var groupCount = 0

    var fff = g.componentTraverser().toList

    for (d <- fff) {
      println("-------------------------------------------------------------------------------------------------------")
      d.nodes.foreach(in => {

        println(gson.toJson(mappedValues.get(in.value.productIterator.next().toString.toInt)))
        objCount += 1
      })
      println("-------------------------------------------------------------------------------------------------------")
      groupCount += 1
    }

    val dateTimeFormat = "yyyy-MM-dd HH:mm:ss.SSSZ"

    println(s"--------------------------------- ${DateTime.now().toString(dateTimeFormat)} -------------------------")
  }
}