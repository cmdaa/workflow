# Running CMDAA

## Running the CMDAA application in your local developer environment. 
  1.  This guide will walk you through setting up the CMDAA application within your local development environment.
  2.  The CMDAA project has many dependencies that it will rely on to run successfuly within your environment.
      * Docker
      * Kubernetes
      * Rancher(optional)
      * Argo workflow engine
      * Minio
      
  *  Recommended hardware requirements for running the CMDAA applicastion.
      * 2.5 GHz 4 core processor
      * 16GB of memory
      * Greater than 20GB of available disk space

## Install Kubernetes on Mac OSX
### Method 1 - Install Docker
   1.  [Install Kubernetes](https://gitlab.com/cmdaa/workflow/blob/master/documentation/INSTALLMAC.md)

### Method 2 - MiniKube
   1.  [Install MiniKube]()

## Install Kubernetes on Linux
   1.  Follow instructions for [Install MiniKube]()

## Install Rancher
   1. [Install Rancher](https://rancher.com/blog/2018/2018-05-18-how-to-run-rancher-2-0-on-your-desktop/)

## Install Argo and Minio
   1.  [Follow These Directions in order to install Argo and Minio](https://github.com/argoproj/argo/blob/master/demo.md)
   2.  The previous step had you point your workflow engine to a default bucket within S3.  We need to point argo to the CMDAA workflow bucket.<br>
```
kubectl edit cm -n argo workflow-controller-configmap
...
apiVersion: v1
...
data:
  config: |
    artifactRepository:
      s3:
        bucket: argo-workflow-bucket
        endpoint: argo-artifacts-minio.default:9000
        insecure: true
        accessKeySecret:
          name: argo-artifacts-minio
          key: accesskey
        secretKeySecret:
          name: argo-artifacts-minio
          key: secretkey
...
kind: ConfigMap
...
```

## Create volume on your Dev environment to map your kubernetes persistent volumes.
   1.  Run the following command in the terminal
       ```
       mkdir /<chosen directory>/kube-data/
       ```
   2. Change the permissions on the directory for read and write for the docker-for-desktop application


   
   2.  The CMDAA project uses the Argo workflow engine to execute it's workflows.  In order for the workflow engine in the CMDAA namespace to execute workflows, the following role binding must be create.<br>
       ```kubectl create rolebinding default-admin --clusterrole=admin --serviceaccount=default:default --namespace=cmdaa```

## Set up your project Namespace.
   ```
   cat > namespace.yml <<EOF
   apiVersion: v1
   kind: Namespace
   metadata:
     name: cmdaa
   EOF
   ```
   Execute the following
   ```
   kubectl create -f namespace.yml
   ```
  
## Set up postgres
   ```
   ```
   
## Project Resources
* CMDAA GitLab:  https://gitlab.com/cmdaa
* CMDAA Slack:   [click here to join](https://join.slack.com/t/cmdaa-workspace/shared_invite/enQtNDM4MzE5NzM4OTY1LTE2MDYwNzE4ODc0NjAzMGMzY2VkMDFkNjM2ODdlNjU0ZjUxODVhMjM5NDM0M2EwZGNiOGZiYmUzOTJmY2Q2YTA)
* CMDAA website: https://cmdaa.io/
 

