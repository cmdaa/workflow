package org.cmdaa.utils

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.junit.Assert._
import org.scalatest.BeforeAndAfterAll
import org.cmdaa.scala.analytics.conf.LogLikelihoodConf
import org.cmdaa.pojos.LogLikelihoodOutput
import org.cmdaa.scala.analytics.LogLikelihood
import scala.io.Source
import scala.collection.parallel.ParMap


@RunWith(classOf[JUnitRunner])
class LoglikelihoodUtilsTest extends FunSuite with BeforeAndAfterAll {
  var conf: LogLikelihoodConf = _
  var output: LogLikelihoodOutput = _
  var wordCount: ParMap[String, Int] = _
  val string = "Following creative tensions, Wright left Pink Floyd in 1979, followed by Waters in 1985. Gilmour and Mason continued as"
  val shortened = "Following creative tensions, Wright left Pink Floyd in NUM, followed by Waters in NUM. Gilmour and Mason continued as"
  val key = "following creative tensions wright pink floyd in num followed by waters in num gilmour and mason continued as"

  override def beforeAll = {
    conf = new LogLikelihoodConf(List("-i", "src/test/resources/log-likelihood.txt", "-o", "", "-l",
      "0.67", "-a", "llh test"))
    output = new LogLikelihood().run(conf)

    wordCount = Source.fromFile(conf.inputDirectory.apply(), "UTF-8").getLines().toStream.
      par.
      flatMap(LoglikelihoodUtils.reformatString(_, conf.removeNonAlpha.apply()).split("\\W+")).
      par.
      map(w => w.toLowerCase).
      par.
      groupBy(w => w).
      par.
      mapValues(_.size)
  }

//  test("test low occurrence") {
//    val result = LoglikelihoodUtils.computeLogLikelihood(2, 100, 8, 1500)
//    assertEquals(0.14394190251805883, result, 0)
//  }
//
//  test("test high occurrence") {
//    val result = LoglikelihoodUtils.computeLogLikelihood(15, 100, 150, 1500)
//    assertEquals(0.13147560918022128, result, 0)
//  }
//
//  test("test reformatString removing directories") {
//    val hdfsDir = "hdfs://dist/spark/2.3.0 fs-operation:read"
//    val fsDir = "file://dist/spark/2.3.0/conf fs-operation:write"
//
//    assertEquals("DIR fsoperationread", LoglikelihoodUtils.reformatString(hdfsDir, true))
//    assertEquals("DIR fsoperationwrite", LoglikelihoodUtils.reformatString(fsDir, true))
//  }
//
//  test("test formatString removeNonAlpha == 0") {
//    val msg = "time=\"2017-12-12T16:29:31Z\" level=info msg=\"Skipping configuration for marathon\""
//    assertEquals("time=\"DATE\" level=info msg=\"Skipping configuration for marathon\"",
//      LoglikelihoodUtils.reformatString(msg, false))
//  }
//
//  test("test formatString removeNonAlpha == 1") {
//    val msg = "time=\"2017-12-12T16:29:31Z\" level=info msg=\"Skipping configuration for marathon\""
//    assertEquals("timeDATE levelinfo msgSkipping configuration for marathon",
//      LoglikelihoodUtils.reformatString(msg, true))
//  }
//
//  test("test processLine") {
//    val line = LoglikelihoodUtils.processLine(string, conf, wordCount, output)
//    assertEquals(string, line.originalMessage)
//    assertEquals("a618c2f0-6de0-3c3e-ab6e-01a640458864", line.hash)
//    assertEquals(key, line.keyString)
//    assertEquals(shortened, line.shortenedMessage)
//    assertEquals("", line.clusterId)
//  }
//
//  test("test generateHash") {
//    val keyString = "following creative tensions wright pink floyd in num followed by waters"
//    assertEquals("b0c1e0a8-7173-3440-bddf-632a2e0063a9", LoglikelihoodUtils.generateHash(keyString))
//  }
//
//  test("test generateKeyString") {
//    val line = LoglikelihoodUtils.processLine(string, conf, wordCount, output)
//    val keyString = LoglikelihoodUtils.generateKeyString(line, conf, wordCount, output)
//    assertEquals(key, keyString)
//  }
}