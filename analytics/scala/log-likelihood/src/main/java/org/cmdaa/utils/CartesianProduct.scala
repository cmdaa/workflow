package org.cmdaa.utils

import org.cmdaa.pojos.LogLikelihoodLine

/**
  * Created on 1/25/18.
  */
class CartesianProduct[A] {
  def cross(a: List[A], b:List[A]) = {
    a.map( p => b.map( o => (p,o))).flatten
  }

  def cross(a: List[A]) = {
    a.map( p => a.map( o => (p,o))).flatten
  }

  def cross(a: Stream[A]) = {
    a.par.map( p => a.par.map( o => (p,o))).flatten
  }
}
