package org.cmdaa.utils.multiline

import scala.collection.mutable

/**
  * Created on 4/4/18.
  */
class ChiValues extends mutable.HashMap[String, Int]{
  override def put(key: String, value: Int) = {
    if( contains(key) ){
      super.put( key, get( key ).get + value )
    }else{
      super.put(key, 0)
    }
  }
}
