package org.cmdaa.pojos

import org.junit.Assert._
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.specs2.codata.Process.Await

import scala.concurrent
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.{Duration, DurationInt}


@RunWith(classOf[JUnitRunner])
class LogLikelihoodOutputTest extends FunSuite {

  protected def getBasicObject: LogLikelihoodOutput = {
    val output = new LogLikelihoodOutput()

    output.totalNaN = 1
    output.totalWords = 10
    output.totalMaxLikelihood = 5
    output.totalLogLines = 5

    output
  }

  test("basic object state") {
    val output = getBasicObject

    assertEquals(1, output.totalNaN)
    assertEquals(10, output.totalWords)
    assertEquals(5, output.totalMaxLikelihood)
    assertEquals(5, output.totalLogLines)
  }

  test("object state with Iterator[LogLikelihoodLine]") {
    val line1 = new LogLikelihoodLine()
    line1.originalMessage = "originalMessage 0"
    line1.shortenedMessage = "shortenedMessage 0"
    line1.keyString = "keyString 0"

    val line2 = new LogLikelihoodLine()
    line2.originalMessage = "originalMessage 1"
    line2.shortenedMessage = "shortenedMessage 1"
    line2.keyString = "keyString 1"

    val line3 = new LogLikelihoodLine()
    line3.originalMessage = "originalMessage 2"
    line3.shortenedMessage = "shortenedMessage 2"
    line3.keyString = "keyString 2"

    val output = getBasicObject

    implicit val ec = ExecutionContext.global

    output.lines = List(Future{line1}, Future{line1}, Future{line1}).iterator

    output.lines.zipWithIndex.foreach { case (l, i) => {
      val line = concurrent.Await.result(l, Duration.Inf)
      assertEquals(s"originalMessage $i", line.originalMessage)
      assertEquals(s"shortenedMessage $i", line.shortenedMessage)
      assertEquals(s"keyString $i", line.keyString)
    }
    }
  }
}