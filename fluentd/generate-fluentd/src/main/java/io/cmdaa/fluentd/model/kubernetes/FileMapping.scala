package io.cmdaa.fluentd.model.kubernetes

object FileMapping {
  case class FileMapping(name: String, containerLocation: String, physicalLocation: String, `type`: String, readOnly: Boolean = true)


  /*
  *   volumes:
    - name: config-volume
      configMap:
        name: special-config
        items:
        - key: SPECIAL_LEVEL
          path: keys
*/
  case class FileMappingConfigMap( name: String, containerLocation: String, configMapName: String, configMapKey: String, newPath: String, readOnly: Boolean = true)
}
